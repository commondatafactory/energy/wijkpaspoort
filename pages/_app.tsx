// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import '../src/css/App.css'
import '../src/components/panels/asset/graphs.css'

import { AppProps } from 'next/app'
import Head from 'next/head'

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  const goatcounterSettings = JSON.stringify({
    allow_local: false,
    path: (path: string) => location.host + path,
    referrer: () => document.referrer,
  })

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="manifest" href="/manifest.json" />

        <title>
          Datavoorziening Wijkpaspoort energietransitie - VNG realisatie
        </title>
        <meta
          name="description"
          content="In een oogopslag zien hoe een wijk er voor staat? En weten wat dat betekent voor de energietransitie? Dat kan met het Wijkpaspoort! Het Wijkpaspoort is een infographic waarin de meest belangrijke data zijn opgenomen die een inzicht geven in de karakteristieken van een bepaalde wijk. Ontwikkeld door VNG realisatie."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#ffbc2c" />
      </Head>

      <script
        data-goatcounter="https://geitjes.commondatafactory.nl/count"
        data-goatcounter-settings={goatcounterSettings}
        async
        src="//geitjes.commondatafactory.nl/count.js"
      />

      <Component {...pageProps} />
    </>
  )
}

export default App
