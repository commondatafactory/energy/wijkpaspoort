// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import '@fontsource/source-sans-pro/latin.css'

import { GlobalStyles } from '@commonground/design-system'
import React, { useState } from 'react'
import { ThemeProvider } from 'styled-components'

import Footer from '../src/components/Footer'
import FooterKadaster from '../src/components/FooterKadaster'
import {
  AppContextProvider,
  FilterTerm,
} from '../src/components/geo-navigatie/GeoNavigatieContextAPI'
import GeoNavigatiePanel from '../src/components/geo-navigatie/GeoNavigatiePanel'
import Header from '../src/components/Header'
import InfoPaneel from '../src/components/InfoPaneel'
import myCustomTheme from '../src/components/theme.js'

function App() {
  const [currentActiveAdminCode, setCurrentActiveAdminCode] = useState('')
  const [currentActiveAdminTerm, setCurrentActiveAdminTerm] =
    useState<FilterTerm>(null)
  const [dataSet, setDataSet] = useState([])

  return (
    <ThemeProvider theme={myCustomTheme}>
      <GlobalStyles />
      <div className="App">
        <AppContextProvider>
          <Header />
          <GeoNavigatiePanel
            setCurrentActiveAdminCode={setCurrentActiveAdminCode}
            setCurrentActiveAdminTerm={setCurrentActiveAdminTerm}
          />
          <FooterKadaster
            dataSet={dataSet}
            regioCode={currentActiveAdminCode}
            regio={currentActiveAdminTerm}
          />
          <InfoPaneel
            currentActiveAdminCode={currentActiveAdminCode}
            currentActiveAdminTerm={currentActiveAdminTerm}
            setDataSet={setDataSet}
          />
        </AppContextProvider>
        <Footer />
      </div>
    </ThemeProvider>
  )
}

export default App
