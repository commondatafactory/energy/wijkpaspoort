// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

type FieldMapFunc = (values: (string | number)[]) => any
type FieldCombo = [...string[], FieldMapFunc]

export type APIData = { [key: string]: string | number }
// A value in a field mapping can also be a field combo. In this case, a
// variable number of string elements that represent API field names is
// followed by a reduce function. This function receives an array of values
// that are retrieved by using the list of field names preceding the function.
// The value returned by the function is used as value in the resulting mapped
// object.
export type FieldMapping = { [key: string]: string | FieldCombo }

export const generateAPIFieldParam = (mapping: FieldMapping) => {
  return Object.values(mapping)
    .reduce((memo, apiFieldName) => {
      if (apiFieldName instanceof Array) {
        return memo.concat(apiFieldName.slice(0, -1))
      } else {
        return memo.concat(apiFieldName)
      }
    }, [])
    .join(',+')
}
export const mapAPIValues = (mapping: FieldMapping, data: APIData) => {
  return Object.entries(mapping).reduce((memo, [key, dataKey]) => {
    if (dataKey instanceof Array) {
      const func = dataKey[dataKey.length - 1] as FieldMapFunc
      memo[key] = func(dataKey.slice(0, -1).map((key: string) => data[key]))
    } else {
      if (!(dataKey in data)) {
        throw Error('Key not found in API data object')
      }

      memo[key] = data[dataKey]
    }

    return memo
  }, {})
}

type In = { [key: string]: number | string | In }
export function cleanDataObject(datasetObject: In): APIData {
  return Object.entries(datasetObject).reduce((acc, [key, value]) => {
    acc[key] =
      value instanceof Object
        ? cleanDataObject(value)
        : value != null
        ? parseFloat(value as string)
        : 0
    return acc
  }, {})
}
