// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { getEnergieArmoedeMonitor } from './getCDF_cbsea'
import { getCdfLevensloopData } from './getCDF_cbslo'
import { getCdfApi } from './getCDF_vboenergie'
import { getWoningVoorraad } from './getCDF_wv'
import { getHVI } from './hvi'
import { getKernCijfersLokaal } from './kerncijfers_lokaal'
import { getKernCijfersRegionaal } from './kerncijfers_regionaal'
import { cleanDataObject } from './util'

export const getAllData = async function (
  paddedAdminCode,
  adminCode,
  adminTerm
) {
  const generalRequests = [
    // Energielabel klasse,
    getCdfApi(adminCode, adminTerm, 'energieklasse'),
    // woningtype voor paneel energie label klasse
    getCdfApi(adminCode, adminTerm, 'woning_type'),
    // aanwezige utiliteitsbouw
    getCdfApi(adminCode, adminTerm, 'gebruiksdoelen-mixed'),
    // Hoofdverwarmingsinstallatie
    getHVI(adminTerm, paddedAdminCode),
  ]
  let extraRequests

  if (adminTerm === 'provinciecode' || adminTerm === 'landcode') {
    extraRequests = [
      // cbs energie verbruik
      getKernCijfersRegionaal(paddedAdminCode),
      // cbs, verdeling woningvoorraad
      getWoningVoorraad(adminCode, adminTerm),
    ]
  } else {
    // gemeentecode, wijkcode or buurtcode
    extraRequests = [
      // cbs energieverbruik & sociale kenmerken
      getKernCijfersLokaal(paddedAdminCode),

      // Woningvoorraad
      getWoningVoorraad(adminCode, adminTerm),
      // Levensloopbestendig wonen
      getCdfLevensloopData(adminCode, 'Type_eigendom_woning'),
      getCdfLevensloopData(adminCode, 'Bouwperiode_woning'),
      // Monitor energie armoede
      getEnergieArmoedeMonitor(adminCode, adminTerm),
    ]
  }

  const [
    // General requests
    energieLabelKlasse,
    woningtype,
    utiliteitsbouw,
    hoofdverwarmingsinstallatie,
    // Extra requests
    energieverbruik,
    woningvoorraad,
    levensloopbestendigwonen1,
    levensloopbestendigwonen2,
    monitorEnergieArmoede,
  ] = await Promise.all([...generalRequests, ...extraRequests])

  // energie dataset
  const energieLabelKlasseDataset = cleanDataObject(energieLabelKlasse)
  //woning type dataset
  const woningTypeDataSet = cleanDataObject(woningtype)

  // all data voor downwload ?
  let allDataObject = {
    ...energieLabelKlasseDataset,
    ...woningTypeDataSet,
    ...utiliteitsbouw,
    ...energieverbruik,
    ...woningvoorraad,
    ea_LIHLEK: monitorEnergieArmoede?.ea_LIHLEK || 0,
  }
  allDataObject = cleanDataObject(allDataObject)
  // console.log(allDataObject)

  return {
    allDataObject,
    energieLabelKlasseDataset,
    woningTypeDataSet,
    hoofdverwarmingsinstallatie,
    levensloopbestendigwonen1,
    levensloopbestendigwonen2,
    monitorEnergieArmoede: monitorEnergieArmoede?.ea_dataSet,
  }
}
