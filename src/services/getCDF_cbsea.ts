// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const getAPIcdfEnergieArmoede = function (regioCode, admin, attrb) {
  const url = 'https://ds.cbsea.commondatafactory.nl/'

  return fetch(`${url}list/?match-${admin}=${regioCode}&groupby=${attrb}`)
    .then((response) => {
      return response.json()
    })
    .then((data) => {
      if (data) {
        return data
      }
    })
    .catch(function () {
      console.error(
        `Oeps, er ging iets fout met het ophalen van de data. \nMogelijk is er een tijdelijke storing bij onze services, of deze ${admin} is niet beschikbaar. \n\n Probeer het later nog een keer! \n Of neem contact op met ons: vip@vng.nl \n\n ${url}/help`
      )
      return {}
    })
}

export const getEnergieArmoedeMonitor = async function (
  regioCode: string,
  admin: 'buurtcode' | 'wijkcode' | 'gemeentecode'
) {
  const energiearmoede = await Promise.all([
    getAPIcdfEnergieArmoede(
      `${regioCode}`,
      `${admin}`,
      'soort_eigendom_woning'
    ),
  ])
  return {
    ea_LIHLEK: energiearmoede[0].Totaal[0].energiearmoede_lihelek2,
    ea_dataSet: energiearmoede[0],
  }
}
