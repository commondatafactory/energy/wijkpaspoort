// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  cleanDataObject,
  FieldMapping,
  generateAPIFieldParam,
  mapAPIValues,
} from './util'

const apiFieldMapping: FieldMapping = {
  Percentage_Stadsverwarming: 'PercentageWoningenMetStadsverwarming_64',

  Gemiddeld_Elektriciteitsverbruik: 'GemiddeldeElektriciteitsleveringTotaal_48',
  Gemiddeld_Elektriciteitsverbruik_Appartement: 'Appartement_49',
  Gemiddeld_Elektriciteitsverbruik_Tussenwoning: 'Tussenwoning_50',
  Gemiddeld_Elektriciteitsverbruik_Hoekwoning: 'Hoekwoning_51',
  Gemiddeld_Elektriciteitsverbruik_2_onder_1_kap: 'TweeOnderEenKapWoning_52',
  Gemiddeld_Elektriciteitsverbruik_Vrijstaand: 'VrijstaandeWoning_53',

  Gemiddeld_Gasverbruik: 'GemiddeldAardgasverbruikTotaal_56',
  Gemiddeld_Gasverbruik_Appartement: 'Appartement_57',
  Gemiddeld_Gasverbruik_Tussenwoning: 'Tussenwoning_58',
  Gemiddeld_Gasverbruik_Hoekwoning: 'Hoekwoning_59',
  Gemiddeld_Gasverbruik_2_onder_1_kap: 'TweeOnderEenKapWoning_60',
  Gemiddeld_Gasverbruik_Vrijstaand: 'VrijstaandeWoning_61',

  AantalInwoners: 'AantalInwoners_5',
  k_0Tot15Jaar: 'k_0Tot15Jaar_8',
  k_15Tot25Jaar: 'k_15Tot25Jaar_9',
  k_25Tot45Jaar: 'k_25Tot45Jaar_10',
  k_45Tot65Jaar: 'k_45Tot65Jaar_11',
  k_65JaarOfOuder: 'k_65JaarOfOuder_12',

  HuishoudensTotaal: 'HuishoudensTotaal_29',
  GemiddeldeHuishoudensgrootte: 'GemiddeldeHuishoudensgrootte_33',
  Eenpersoonshuishoudens: 'Eenpersoonshuishoudens_30',
  HuishoudensZonderKinderen: 'HuishoudensZonderKinderen_31',
  HuishoudensMetKinderen: 'HuishoudensMetKinderen_32',

  GemiddeldInkomenPerInwoner: 'GemiddeldInkomenPerInwoner_78',
  k_40HuishoudensMetLaagsteInkomen: 'k_40HuishoudensMetLaagsteInkomen_82',
  k_20HuishoudensMetHoogsteInkomen: 'k_20HuishoudensMetHoogsteInkomen_83',

  GemiddeldeWOZWaardeVanWoningen: 'GemiddeldeWOZWaardeVanWoningen_36',
}

export const getKernCijfersLokaal = async function (paddedAdminCode: string) {
  const fieldNames = generateAPIFieldParam(apiFieldMapping)
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/85618NED/UntypedDataSet?$filter=((WijkenEnBuurten+eq+'${paddedAdminCode}'))&$select=${fieldNames}`
  )
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response
    })
    .then((response) => response.json())
    .then((data) => {
      if (!data.value.length) {
        return null
      }

      data = data.value[0]
      return cleanDataObject(mapAPIValues(apiFieldMapping, data))
    })
    .catch(function () {
      console.error(
        'Oeps, er ging iets fout met het ophalen van de data bij het CBS.'
      )
      return {}
    })
}
