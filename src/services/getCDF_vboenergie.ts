// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { editELabelData } from '../utils/helper'

//  CDF API Elabels & BAG
export const getCdfApi = function (regioCode, admin, groupby) {
  const url = 'https://ds.vboenergie.commondatafactory.nl'
  return fetch(
    `${url}/list/?match-${admin}=${regioCode}&groupby=${groupby}&reduce=count`
  )
    .then((response) => {
      // getTotal(response.headers.get('total-items'))
      return response.json()
    })
    .then((data: { [key: string]: { count: string } }) => {
      if (!groupby.indexOf('labelscore')) {
        return editELabelData(data, groupby)
      } else {
        return Object.entries(data).reduce(
          (acc, [key, { count }]) => ({
            ...acc,
            [key === '' ? 'overigLabel' : key]: count,
          }),
          {}
        )
      }
    })
    .catch(function () {
      console.error(
        `Oeps, er ging iets fout met het ophalen van de data. \nMogelijk is er een tijdelijke storing bij onze services, of deze ${admin} is niet beschikbaar. \n\n Probeer het later nog een keer! \n Of neem contact op met ons: vip@vng.nl \n\n ${url}/help`
      )
      return {}
    })
}
