// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  cleanDataObject,
  FieldMapping,
  generateAPIFieldParam,
  mapAPIValues,
} from './util'

export const getKernCijfersRegionaal = async function (
  paddedAdminCode: string
) {
  return Promise.all([
    getLimitedKernCijfersRegionaal(paddedAdminCode),
    getEnergieRegionaal(paddedAdminCode),
  ]).then(([kernCijfers, energie]) => {
    return {
      ...kernCijfers,
      ...energie,
    }
  })
}

const sum = (values: string[]): number =>
  values.reduce((memo, n: string) => memo + parseFloat(n), 0)

const apiFieldMapping: FieldMapping = {
  AantalInwoners: 'TotaleBevolking_1',

  k_0Tot15Jaar: ['JongerDan5Jaar_4', 'k_5Tot10Jaar_5', 'k_10Tot15Jaar_6', sum],
  k_15Tot25Jaar: ['k_15Tot20Jaar_7', 'k_20Tot25Jaar_8', sum],
  k_25Tot45Jaar: 'k_25Tot45Jaar_9',
  k_45Tot65Jaar: 'k_45Tot65Jaar_10',
  k_65JaarOfOuder: ['k_65Tot80Jaar_11', 'k_80JaarOfOuder_12', sum],

  HuishoudensTotaal: 'TotaalParticuliereHuishoudens_82',
  GemiddeldeHuishoudensgrootte: 'GemiddeldeHuishoudensgrootte_89',
  Eenpersoonshuishoudens: 'Eenpersoonshuishoudens_83',
  HuishoudensZonderKinderen: 'MeerpersoonshuishoudensZonderKinderen_84',
  HuishoudensMetKinderen: 'MeerpersoonshuishoudensMetKinderen_85',

  GemiddeldeWOZWaardeVanWoningen: 'GemiddeldeWOZWaardeVanWoningen_98',
}

// Limited is in the name because this API is missing energy data. We get the
// energy data by making an additional request to a different API. See below.
const getLimitedKernCijfersRegionaal = async function (
  paddedAdminCode: string
) {
  const fieldNames = generateAPIFieldParam(apiFieldMapping)
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/70072NED/UntypedDataSet?$filter=((Perioden+eq+'2023JJ00'))+and+((RegioS+eq+'${paddedAdminCode}'))&$select=${fieldNames}`
  )
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response
    })
    .then((response) => response.json())
    .then((data) => {
      if (!data.value.length) {
        return null
      }

      data = data.value[0]
      return cleanDataObject(mapAPIValues(apiFieldMapping, data))
    })
    .catch(function () {
      console.error(
        'Oeps, er ging iets fout met het ophalen van de data bij het CBS.'
      )
      return {}
    })
}

async function getEnergieRegionaal(paddedAdminCode: string) {
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/81528NED/UntypedDataSet?$filter=((Perioden eq '2023JJ00')) and ((RegioS eq '${paddedAdminCode}')))&$select=Woningkenmerken, Perioden, GemiddeldAardgasverbruik_1, GemiddeldeElektriciteitslevering_2, Stadsverwarming_4`
  )
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response
    })
    .then((response) => response.json())
    .then((data) => {
      const woningTypePostfix = {
        T001100: null, // Average over total, no postfix
        ZW25810: 'Appartement',
        ZW25805: 'Tussenwoning',
        ZW25806: 'Hoekwoning',
        ZW10300: '2_onder_1_kap',
        ZW10320: 'Vrijstaand',
      }
      const result = {
        Percentage_Stadsverwarming: 0,

        Gemiddeld_Elektriciteitsverbruik: 0,
        Gemiddeld_Elektriciteitsverbruik_Appartement: 0,
        Gemiddeld_Elektriciteitsverbruik_Tussenwoning: 0,
        Gemiddeld_Elektriciteitsverbruik_Hoekwoning: 0,
        Gemiddeld_Elektriciteitsverbruik_2_onder_1_kap: 0,
        Gemiddeld_Elektriciteitsverbruik_Vrijstaand: 0,

        Gemiddeld_Gasverbruik: 0,
        Gemiddeld_Gasverbruik_Appartement: 0,
        Gemiddeld_Gasverbruik_Tussenwoning: 0,
        Gemiddeld_Gasverbruik_Hoekwoning: 0,
        Gemiddeld_Gasverbruik_2_onder_1_kap: 0,
        Gemiddeld_Gasverbruik_Vrijstaand: 0,
      }

      data.value.forEach((entry: any) => {
        if (!(entry.Woningkenmerken in woningTypePostfix)) {
          return
        }

        const postfix = woningTypePostfix[entry.Woningkenmerken]
        const key_gas_base = 'Gemiddeld_Gasverbruik'
        const key_energy_base = 'Gemiddeld_Elektriciteitsverbruik'
        const key_gas = postfix ? `${key_gas_base}_${postfix}` : key_gas_base
        const key_energy = postfix
          ? `${key_energy_base}_${postfix}`
          : key_energy_base

        result[key_gas] = parseFloat(entry['GemiddeldAardgasverbruik_1'])
        result[key_energy] = parseFloat(
          entry['GemiddeldeElektriciteitslevering_2']
        )
        if (!postfix) {
          result['Percentage_Stadsverwarming'] = parseFloat(
            entry['Stadsverwarming_4']
          )
        }
      })

      return result
    })
    .catch(function () {
      console.error(
        'Oeps, er ging iets fout met het ophalen van de data bij het CBS. \nMogelijk is er een storing bij de services van het CBS. \nKijk op https://opendata.cbs.nl/statline/#/CBS/nl/dataset/81528NED/table?ts=1598523217901 voor de bron data. \n \nOf, probeer het nog een keer! '
      )
      return {}
    })
}
