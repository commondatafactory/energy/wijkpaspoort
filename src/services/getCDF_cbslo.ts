// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const getCdfLevensloopData = async function (
  regioCode: string,
  attrb: string
) {
  return fetch(
    `https://ds.cbslo.commondatafactory.nl/list/?match-Regiocode=${regioCode}&groupby=${attrb}`
  )
    .then((response) => {
      return response.json()
    })
    .then((data) => {
      if (data) {
        return data
      }
    })
    .catch(function (err) {
      console.error(
        `Oeps, er ging iets fout met het ophalen van de data. (${err.message})`
      )

      return {}
    })
}
