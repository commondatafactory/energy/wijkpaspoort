// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { cleanDataObject } from './util'

// Result parsers
// --------------
type InputData = { [key: string]: object | object[] }
type OutputData = { data: { [key: string]: number }; totaal: number }
type ParseFn = (data: InputData) => OutputData

const reduceToCount = (countBy?: string): ParseFn => {
  return (data: InputData) => {
    return Object.keys(data).reduce(
      (result: any, key: string) => {
        const element = data[key]
        const memoKey = key

        if (element instanceof Array) {
          // This is an array of data objects. We need a prop that holds the
          // relevant value to count.
          if (!countBy) {
            throw Error('Huh?')
          }
          result.data[memoKey] = element.reduce<number>(
            (memo: number, obj: any) => {
              return memo + parseInt(obj[countBy])
            },
            0
          )
        } else if (element instanceof Object) {
          // This is a data object holding count values. When a countProp is not
          // provided, sum all properties.
          if (countBy) {
            result.data[memoKey] = parseInt(element[countBy] ?? null)
            return result
          } else {
            result.data[memoKey] = Object.values(element).reduce(
              (memo: number, value: string) => {
                return memo + parseInt(value)
              },
              0
            )
          }
        }

        result.totaal += result.data[memoKey]
        return result
      },
      { data: {}, totaal: 0 }
    )
  }
}

type PropCheckFn = (key: string) => boolean
const pick = (prop: string | PropCheckFn): ParseFn => {
  return (data: { [key: string]: object }) => {
    return Object.entries(data).reduce(
      (memo, [key, value]) => {
        if (typeof prop === 'string' && prop in value) {
          memo.data[key] = parseInt(value[prop])
        } else if (prop instanceof Function) {
          memo.data[key] = Object.entries(value).reduce<number>(
            (memo, [key, value]) => {
              if (!prop(key)) {
                return memo
              }
              return memo + parseInt(value)
            },
            0
          )
        }

        memo.totaal += memo.data[key] ?? 0
        return memo
      },
      { data: {}, totaal: 0 }
    )
  }
}

// API fetchers
// ------------
const fetchAPI = async function (
  query: {
    source?: string
    regioCode: string
    admin: 'buurtcode' | 'wijkcode' | 'gemeentecode'
    groupBy?: string
    reduceBy?: string
    matchBy?: string
  },
  parseResult?: (data: object) => object
) {
  const { source, regioCode, admin, groupBy, reduceBy, matchBy } = query
  const url = `https://acc.ds.eigendom${source ?? ''}.commondatafactory.nl`

  return fetch(
    `${url}/list/?match-${admin}=${regioCode}&groupby=${groupBy ?? ''}&reduce=${
      reduceBy ?? ''
    }${matchBy ?? ''}`
  )
    .then((response) => response.json())
    .then((data) => {
      data ??= {}
      return parseResult ? parseResult(data) : { data }
    })
    .catch(function () {
      console.error(`Er ging iets fout met het ophalen van de data.`)
      return { data: {}, totaal: 0 }
    })
}

// Combine all API calls into one consistent data set. When one of the APIs
// changes its output format, this is the place to change it.
export const getWoningVoorraad = async function (
  regioCode: string,
  admin: 'buurtcode' | 'wijkcode' | 'gemeentecode'
) {
  // Fetch the raw data...
  const [woningTypeData, bouwperiodeData] = (await Promise.all([
    fetchAPI({ source: 'wto', regioCode, admin, reduceBy: 'woningtype' }),
    fetchAPI({
      source: 'bouw',
      regioCode,
      admin,
      reduceBy: 'eigenaar_bouwjaar',
    }),
  ])) as any[]

  // ... and shuffle it a few times to meet different needs.
  const woningType = reduceToCount()(woningTypeData.data)
  const bouwperiode = reduceToCount()(bouwperiodeData.data)

  const corporatieBouwperiode = pick('Woningcorporatie')(bouwperiodeData.data)
  const particulierBouwperiode = pick('Particuliere koopwoning')(
    bouwperiodeData.data
  )
  const overigHuurBouwperiode = pick((key) => key.startsWith('Overig (huur)'))(
    bouwperiodeData.data
  )
  const onbekendBouwperiode = pick('Geen eenduidige koppeling')(
    bouwperiodeData.data
  )

  const coorporatieTypeWoning = pick('Woningcorporatie')(woningTypeData.data)
  const particulierTypeWoning = pick('Particuliere koopwoning')(
    woningTypeData.data
  )
  const overigHuurTypeWoning = pick((key) => key.startsWith('Overig (huur)'))(
    woningTypeData.data
  )
  const onbekendTypeWoning = pick('Geen eenduidige koppeling')(
    woningTypeData.data
  )

  // This gets passed to the Woningvoorraad panel component.
  const allData = {
    wv_aantal_TypeWoning_Appartement: woningType?.data['Appartement'],
    wv_aantal_TypeWoning_Hoekwoning: woningType?.data['Hoekwoning'],
    wv_aantal_TypeWoning_Overig: woningType?.data['0'],
    wv_aantal_TypeWoning_Tussenwoning: woningType?.data['Tussenwoning'],
    wv_aantal_TypeWoning_TweeOnderEenKap: woningType?.data['2-onder-1-kap'],
    wv_aantal_TypeWoning_VrijstaandeWoning: woningType?.data['Vrijstaand'],

    wv_aantal_Bouwperiode_Voor_1946: bouwperiode?.data['Voor 1946'],
    wv_aantal_Bouwperiode_1946_1974: bouwperiode?.data['1946 t/m 1974'],
    wv_aantal_Bouwperiode_1975_1991: bouwperiode?.data['1975 t/m 1991'],
    wv_aantal_Bouwperiode_1992_2006: bouwperiode?.data['1992 t/m 2006'],
    wv_aantal_Bouwperiode_2006_later: bouwperiode?.data['na 2006'],

    wv_aantal_Cooperatie_Bouwperiode_Voor_1946:
      corporatieBouwperiode.data['Voor 1946'],
    wv_aantal_Cooperatie_Bouwperiode_1946_1974:
      corporatieBouwperiode.data['1946 t/m 1974'],
    wv_aantal_Cooperatie_Bouwperiode_1975_1991:
      corporatieBouwperiode.data['1975 t/m 1991'],
    wv_aantal_Cooperatie_Bouwperiode_1992_2006:
      corporatieBouwperiode.data['1992 t/m 2006'],
    wv_aantal_Cooperatie_Bouwperiode_2006_later:
      corporatieBouwperiode.data['na 2006'],

    wv_aantal_Particulier_Bouwperiode_Voor_1946:
      particulierBouwperiode?.data['Voor 1946'],
    wv_aantal_Particulier_Bouwperiode_1946_1974:
      particulierBouwperiode?.data['1946 t/m 1974'],
    wv_aantal_Particulier_Bouwperiode_1975_1991:
      particulierBouwperiode?.data['1975 t/m 1991'],
    wv_aantal_Particulier_Bouwperiode_1992_2006:
      particulierBouwperiode?.data['1992 t/m 2006'],
    wv_aantal_Particulier_Bouwperiode_2006_later:
      particulierBouwperiode?.data['na 2006'],

    wv_aantal_Cooperatie_TypeWoning_Appartement:
      coorporatieTypeWoning?.data['Appartement'],
    wv_aantal_Cooperatie_TypeWoning_Hoekwoning:
      coorporatieTypeWoning?.data['Hoekwoning'],
    wv_aantal_Cooperatie_TypeWoning_Overig: coorporatieTypeWoning?.data['0'],
    wv_aantal_Cooperatie_TypeWoning_Tussenwoning:
      coorporatieTypeWoning?.data['Tussenwoning'],
    wv_aantal_Cooperatie_TypeWoning_TweeOnderEenKap:
      coorporatieTypeWoning?.data['2-onder-1-kap'],
    wv_aantal_Cooperatie_TypeWoning_VrijstaandeWoning:
      coorporatieTypeWoning?.data['Vrijstaand'],

    wv_aantal_Particulier_TypeWoning_Appartement:
      particulierTypeWoning?.data['Appartement'],
    wv_aantal_Particulier_TypeWoning_Hoekwoning:
      particulierTypeWoning?.data['Hoekwoning'],
    wv_aantal_Particulier_TypeWoning_Overig: particulierTypeWoning?.data['0'],
    wv_aantal_Particulier_TypeWoning_Tussenwoning:
      particulierTypeWoning?.data['Tussenwoning'],
    wv_aantal_Particulier_TypeWoning_TweeOnderEenKap:
      particulierTypeWoning?.data['2-onder-1-kap'],
    wv_aantal_Particulier_TypeWoning_VrijstaandeWoning:
      particulierTypeWoning?.data['Vrijstaand'],

    wv_totaal_WoningVoorraad: bouwperiode?.totaal || woningType?.totaal,
    wv_totaal_TypeEigenaar_CorporatieBezit:
      corporatieBouwperiode?.totaal || coorporatieTypeWoning?.totaal,
    wv_totaal_TypeEigenaar_ParticulierBezit:
      particulierBouwperiode?.totaal || particulierTypeWoning?.totaal,
    wv_totaal_TypeEigenaar_OverigHuurBezit:
      overigHuurBouwperiode?.totaal || overigHuurTypeWoning?.totaal,
    wv_totaal_TypeEigenaar_OverigBezit:
      onbekendBouwperiode?.totaal || onbekendTypeWoning?.totaal,

    wv_percentage_TypeEigenaar_CorporatieBezit:
      Math.round((100 * corporatieBouwperiode?.totaal) / bouwperiode?.totaal) ||
      0,
    wv_percentage_TypeEigenaar_ParticulierBezit:
      Math.round(
        (100 * particulierBouwperiode?.totaal) / bouwperiode?.totaal
      ) || 0,
    wv_percentage_TypeEigenaar_OverigHuurBezit:
      Math.round((100 * overigHuurBouwperiode?.totaal) / bouwperiode?.totaal) ||
      0,
    wv_percentage_TypeEigenaar_OverigBezit:
      Math.round((100 * onbekendBouwperiode?.totaal) / bouwperiode?.totaal) ||
      0,
  }

  // debugger

  return cleanDataObject(allData)
}
