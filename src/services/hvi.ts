// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  APIData,
  FieldMapping,
  generateAPIFieldParam,
  mapAPIValues,
} from './util'

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

// This API is also used in the Hoofdverwarming panel to display all types.
export const getTypeVerwarmingsinstallatie = async function () {
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/84948NED/TypeVerwarmingsinstallatie`
  )
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response
    })
    .then((response) => response.json())
    .then((data) => {
      return data?.value || []
    })
}

// Depending on the adminTerm, this function calls an API that fetches either
// regional or local data.
export const getHVI = async function (
  adminTerm: string,
  paddedAdminCode?: string
) {
  if (adminTerm === 'landcode') {
    paddedAdminCode = 'NL00  '
  }

  const getHVIData =
    adminTerm === 'landcode' || adminTerm === 'provinciecode'
      ? getHVIRegional
      : getHVILocal

  return Promise.all([
    getTypeVerwarmingsinstallatie(),
    getHVIData(paddedAdminCode),
  ])
    .then(([typeList, hvi]) => {
      let dataSet = hvi
      if (!typeList.length || !dataSet) {
        return []
      }

      dataSet = dataSet.map((entry: any): APIData => {
        const nr = parseInt(entry.Woningen)
        const type = typeList.find(
          (t: any) => t.Key === entry.TypeVerwarmingsinstallatie
        )
        return {
          // Only use the first word of the title. `key` is mainly used as a
          // CSS class name.
          key: type.Title.replace(/ .*/, ''),
          value: nr,
          title: type.Title,
        }
      })

      return dataSet
    })
    .catch(function (err) {
      console.error(
        `Oeps, er ging iets fout met het ophalen van de data bij het CBS. (${err.message})`
      )
      return {}
    })
}

// API functions
// -------------
const toNumber = ([value]) => parseFloat(value)

const apiFieldMappingReg: FieldMapping = {
  TypeVerwarmingsinstallatie: 'TypeVerwarmingsinstallatie',
  RegioS: 'RegioS',
  Woningen: ['Woningen_1', toNumber],
}
const getHVIRegional = async function (paddedAdminCode: string) {
  const fieldNames = generateAPIFieldParam(apiFieldMappingReg)
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/84948NED/UntypedDataSet?$filter=((Perioden+eq+%272021JJ00%27))+and+((RegioS+eq+%27${paddedAdminCode}%27))&$select=${fieldNames}`
  ).then(handleAPIResponse(apiFieldMappingReg))
}

const apiFieldMappingLocal: FieldMapping = {
  TypeVerwarmingsinstallatie: 'TypeVerwarmingsinstallatie',
  WijkenEnBuurten: 'WijkenEnBuurten',
  Woningen: ['Woningen_5', toNumber],
}
const getHVILocal = async function (paddedAdminCode: string) {
  const fieldNames = generateAPIFieldParam(apiFieldMappingLocal)
  return fetch(
    `https://opendata.cbs.nl/ODataApi/odata/85677NED/UntypedDataSet?$filter=((WijkenEnBuurten+eq+%27${paddedAdminCode}%27))&$select=${fieldNames}`
  ).then(handleAPIResponse(apiFieldMappingLocal))
}

const handleAPIResponse = (fieldMapping: FieldMapping) => {
  return async (response: Response): Promise<APIData[]> => {
    return Promise.resolve(response)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        if (!data.value.length) {
          return null
        }

        return data.value.map((entry: APIData) =>
          mapAPIValues(fieldMapping, entry)
        )
      })
  }
}
