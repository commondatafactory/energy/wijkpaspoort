// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { func, string } from 'prop-types'
import React, { useEffect, useState } from 'react'
import Masonry from 'react-masonry-css'

import { getAllData } from '../services/getAllData'
import { stringFillup } from '../utils/helper'
import PanelArmoede from './panels/custom/EnergieArmoede'
import PanelEK from './panels/custom/EnergielabelKlasse'
import PanelHVI from './panels/custom/Hoofdverwarming'
import PanelLevensloop from './panels/custom/Levensloop'
import PanelS from './panels/custom/Sociaal'
import PanelU from './panels/custom/Utiliteit'
import PanelV from './panels/custom/Verbruik'
import PanelW from './panels/custom/Woningvoorraad'
import Panel from './panels/Panel'

function InfoPaneel({
  currentActiveAdminCode,
  currentActiveAdminTerm,
  setDataSet,
}) {
  const [paddedRegioCode, setPaddedRegioCode] = useState('')
  const [dataSet, setData] = useState({})
  const [dataSetWoningType, setDataWoningType] = useState({})
  const [dataSetEnergieKlasse, setDataSetEnergieKlasse] = useState({})
  const [dataSetHVI, setDataSetHVI] = useState({})
  const [dataSetLevensloop1, setDataLevensloop1] = useState({})
  const [dataSetLevensloop2, setDataLevensloop2] = useState({})
  const [dataEnergieArmoede, setDataEnergieArmoede] = useState({})

  useEffect(() => {
    if (currentActiveAdminCode.length > 0) {
      // Setting the right term and code to request the data on.
      if (currentActiveAdminTerm === 'landcode') {
        setPaddedRegioCode(stringFillup(currentActiveAdminCode, 6))
      } else if (currentActiveAdminTerm === 'provinciecode') {
        setPaddedRegioCode(stringFillup(currentActiveAdminCode, 6))
      } else {
        setPaddedRegioCode(stringFillup(currentActiveAdminCode, 10))
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentActiveAdminCode])

  useEffect(() => {
    if (paddedRegioCode) {
      ;(async () => {
        let {
          allDataObject,
          energieLabelKlasseDataset,
          woningTypeDataSet,
          hoofdverwarmingsinstallatie,
          levensloopbestendigwonen1,
          levensloopbestendigwonen2,
          monitorEnergieArmoede,
        } = await getAllData(
          paddedRegioCode,
          currentActiveAdminCode,
          currentActiveAdminTerm
        )
        setData(allDataObject)
        setDataSet(allDataObject)
        setDataSetEnergieKlasse(energieLabelKlasseDataset)
        setDataWoningType(woningTypeDataSet)
        setDataSetHVI(hoofdverwarmingsinstallatie)
        setDataLevensloop1(levensloopbestendigwonen1)
        setDataLevensloop2(levensloopbestendigwonen2)
        setDataEnergieArmoede(monitorEnergieArmoede)
      })()
    }
  }, [paddedRegioCode])

  const breakpointColumnsObj = {
    default: 3,
    1824: 2,
    1224: 1,
  }
  return (
    <>
      <div
        className="panel-layout"
        role="region"
        aria-label="Infographic panelen met verschillende thema's en data relevant voor de energietransitie"
      >
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
          role="list"
        >
          <Panel
            key="woningvoorraad"
            thema="woningvoorraad"
            title="De Woningvoorraad"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/huis.svg"
          >
            <PanelW
              regio={currentActiveAdminTerm}
              regioCode={currentActiveAdminCode}
              dataSet={dataSet}
            />
          </Panel>
          <Panel
            key="verbruik"
            thema="verbruik"
            title="Energie Verbruik"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/gas.svg"
          >
            <PanelV regioCode={currentActiveAdminCode} dataSet={dataSet} />
          </Panel>
          <Panel
            key="hvi"
            thema="hvi"
            title="Hoofdverwarmingsinstallatie woningen"
            regioCode={currentActiveAdminCode}
            regio={currentActiveAdminTerm}
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/gas.svg"
          >
            <PanelHVI
              regioCode={currentActiveAdminCode}
              regio={currentActiveAdminTerm}
              dataSet={dataSetHVI}
            />
          </Panel>

          <Panel
            key="sociaal"
            thema="sociaal"
            title="Sociale kenmerken"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/sociaal.svg"
          >
            <PanelS regioCode={currentActiveAdminCode} dataSet={dataSet} />
          </Panel>

          <Panel
            key="armoede"
            thema="armoede"
            title="Monitor Energiearmoede"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/sociaal.svg"
          >
            <PanelArmoede
              regioCode={currentActiveAdminCode}
              regio={currentActiveAdminTerm}
              dataSet={dataEnergieArmoede}
            />
          </Panel>

          <Panel
            key="levensloop"
            thema="levensloop"
            title="Levensloopbestendig wonen"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/huis.svg"
          >
            <PanelLevensloop
              regioCode={currentActiveAdminCode}
              regio={currentActiveAdminTerm}
              woningTypeDataSet={dataSetLevensloop1}
              bouwPeriodeDataSet={dataSetLevensloop2}
            />
          </Panel>
          <Panel
            key="eklasse"
            thema="eklasse"
            title="Energie Label Klasse"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/elabel.svg"
          >
            <PanelEK
              regioCode={currentActiveAdminCode}
              regio={currentActiveAdminTerm}
              dataSet={dataSetEnergieKlasse}
              dataSetWoningType={dataSetWoningType}
            />
          </Panel>
          <Panel
            key="utiliteit"
            thema="utiliteit"
            title="Aanwezige Utiliteitsbouw"
            currentActiveAdminCode={paddedRegioCode}
            dataSet={dataSet}
            icon="/img/huis.svg"
          >
            <PanelU
              regioCode={currentActiveAdminCode}
              regio={currentActiveAdminTerm}
              dataSet={dataSet}
            />
          </Panel>
        </Masonry>
      </div>
    </>
  )
}

InfoPaneel.propTypes = {
  currentActiveAdminCode: string,
  currentActiveAdminTerm: string,
  setDataSet: func,
}
export default InfoPaneel
