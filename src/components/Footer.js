// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { CGLogo, Drawer, GitLabLogo, Icon } from '@commonground/design-system'
import React, { useState } from 'react'
import styled from 'styled-components'

const FooterContainer = styled.div`
  grid-row: 4 / span 1;
  grid-column: 1 / span 1;
  justify-self: stretch;
  background: ${(div) => div.theme.tokens.colorPaletteGray600};
  color: white;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  font-size: 0.8rem;
  padding: ${(div) => div.theme.tokens.spacing07};
`
const LinksContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  span {
    margin: ${(div) => div.theme.tokens.spacing03};
  }
`
const StyledImg = styled.img`
  max-height: 35px;
  margin: ${(div) => div.theme.tokens.spacing02};
`

const Info = () => (
  <svg viewBox="0 0 24 24" width="14" height="14">
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      fill="currentColor"
      d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM11 7h2v2h-2V7zm0 4h2v6h-2v-6z"
    />
  </svg>
)

function Footer() {
  const [showDrawer, setShowDrawer] = useState(false)

  return (
    <>
      <FooterContainer>
        <LinksContainer
          onClick={() => setShowDrawer(true)}
          style={{ cursor: 'pointer' }}
        >
          <Icon as={Info} />
          <span>Colofon & Uitleg</span>
        </LinksContainer>
        <LinksContainer style={{ borderLeft: 'white solid 1px', width: '3px' }}>
          {' '}
          <h1> </h1>
        </LinksContainer>
        <LinksContainer>
          <span> Een initiatief van:</span>
          <StyledImg src="/img/vng-logo.svg" alt="Logo van VNG Realisatie" />
          <StyledImg src="/img/kadaster_logo.png" alt="Logo Kadaster" />
        </LinksContainer>
      </FooterContainer>
      {showDrawer && (
        <Drawer closeHandler={() => setShowDrawer(false)} autoFocus>
          <Drawer.Header title="Colofon" />
          <Drawer.Content>
            <div className="part">
              <p>
                De Datavoorziening Wijkpaspoort Warmtetransitie is tot stand
                gekomen door een samenwerking tussen het Kadaster en VNG. Het
                Kadaster en VNG zetten zich beide in om gemeenten te
                ondersteunen bij het datagedreven werken aan de energietransitie
                in de gebouwde omgeving, en hechten hierbij aan de principes van
                o.a. Common Ground.
              </p>
              <div className="logos">
                <img
                  src="/img/kadaster_logo.png"
                  className="logo"
                  alt="Logo Kadaster"
                />
                <img
                  src="/img/vng_logo_300.png"
                  className="logo"
                  alt="Logo van VNG "
                />
              </div>
            </div>
            <div className="part">
              <h2>Datavoorziening Wijkpaspoort </h2>
              <p>
                Met deze datavoorziening wordt het nog gemakkelijker werken met
                het Wijkpaspoort Warmtetransitie. Het Wijkpaspoort helpt je
                keuzes te maken en het proces in de wijk te starten voor de
                Warmtetransitie.
              </p>

              <p>Deze datavoorziening bied je twee opties:</p>
              <ol>
                <li>
                  Vul met behulp va de downloadservice het complete Wijkpaspoort
                  voor een wijk naar keuze met gebruik van het landelijk format.
                  Download daarvoor het kwantitatieve deel naar Excel via de
                  downloadknop en vul de kwalitatieve zelf aan met de
                  beschikbare toelichting. Deze manier van werken zorgt voor een
                  totaal overzicht van de situatie in de buurt of wijk.
                </li>
                <li>
                  {' '}
                  Kijk in de viewer voor een wijk naar keuze voor een eerste
                  indruk van de kwantitatieve data van een buurt of wijk. Dit
                  geeft minder diepgang dan het complete Wijkpaspoort, maar kan
                  handig zijn als je voor een groter aantal wijken of buurten
                  beknopte informatie nodig hebt.
                </li>
              </ol>
            </div>
            <div className="part">
              <h2>Contact</h2>
              <p>
                Meer weten over de inhoud? Technische of problemen met de
                applicatie? Mail dan naar{' '}
                <b className="underline">vip@vng.nl</b>
              </p>
              <p>
                <b>Team Verbeteren Informatie Positie, VNG Realisatie</b>
              </p>
            </div>
            <div className="part">
              <p>
                <span style={{ marginRight: '8px' }}>
                  De code kunt u vinden op:{' '}
                </span>
                <a
                  target="_blank"
                  href="https://gitlab.com/commondatafactory/"
                  rel="noreferrer"
                >
                  {' '}
                  <GitLabLogo style={{ height: '24px' }} /> Gitlab{' '}
                </a>{' '}
              </p>

              <p>
                <span style={{ marginRight: '8px' }}>
                  {' '}
                  Ontwikkeling is volgens de implementatie van:{' '}
                </span>
                <a
                  target="_blank"
                  href="https://commonground.nl/"
                  rel="noreferrer"
                >
                  Commonground.nl
                </a>{' '}
                en de{' '}
                <a href="https://standard.publiccode.net/">
                  standard for public code
                </a>
              </p>
            </div>
          </Drawer.Content>
        </Drawer>
      )}
    </>
  )
}
export default Footer
