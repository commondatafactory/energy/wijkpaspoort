// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import styled from 'styled-components'

import { AppContext } from './geo-navigatie/GeoNavigatieContextAPI'
import LocationNavListItem from './LocationNavListItem'

const ControlContainer = styled.div`
  z-index: 10;
  /* min-height: 5vh; */
  @media screen and (max-width: 768px) {
    margin-bottom: ${(div) => div.theme.tokens.spacing03};
  }
  input[type='text'] {
    background-color: ${(div) => div.theme.tokens.colorBackground};
    padding: ${(div) => div.theme.tokens.spacing04};
    border: ${(div) => div.theme.tokens.colorPaletteGray500} 1px solid;
    outline: none;
    z-index: 10;
    width: 100%;
    /* background: url('../img/') no-repeat scroll 7px 7px; */
    @media screen and (max-width: 768px) {
      padding: ${(div) => div.theme.tokens.spacing03};
    }
  }

  #control-input {
    color: ${(p) => p.theme.tokens.colorText};
  }
  #control-input:focus {
    outline: 2px ${(p) => p.theme.tokens.colorInfo} solid;
  }
`
const ResultList = styled.ul`
  padding-left: 0px;
  position: relative;
  background-color: ${(div) => div.theme.tokens.colorBackground};
  border-radius: 0 0 5px 5px;
  box-shadow: 0 1px 5px ${(div) => div.theme.tokens.colorPaletteGray600};
  margin-left: 0;
  z-index: 10;
  margin: 0;
  padding: 0;
  li {
    @media screen and (max-width: 768px) {
      padding: ${(div) => div.theme.tokens.spacing03};
    }
  }
  .result-selected {
    font-weight: bold;
  }

  .result-list li:hover {
    font-weight: bold;
  }
`

// LocationNav is the component of the text bar in which you can type, and upon selecting one of the results, it returns the geometry of the municipality.
class LocationNav extends React.Component {
  state = {
    value: {
      weergavenaam: '',
    },
    resultList: [],
    cursorPosition: -1,
  }

  // Compare typed text with the names of municipalities listed in Nationaal Georegister
  // Store the relevant municipalities and show them in the dropdown menu
  handleChange = (e) => {
    this.setState({ value: { weergavenaam: e.target.value || '' } })
    if (e.target.value.length >= 2) {
      fetch(
        `https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?rows=10&fq=type:(gemeente OR wijk OR buurt)&q=${encodeURIComponent(
          e.target.value
        )}`
      )
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          this.setState({ resultList: data.response.docs })
        })
    }
  }

  // Retrieve the name of the chose municipality and geometry from Nationaal Georegister
  // Change map view accordingly
  lookupLocation = (item) => {
    fetch(
      `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?fl=type,provinciecode,provincienaam,gemeentecode,gemeentenaam,wijkcode,wijknaam,buurtcode,buurtnaam&id=${encodeURIComponent(
        item.id
      )}`
    ) // id,identificatie,type,weergavenaam,gemeentecode,gemeentenaam,provinciecode,provincienaam
      .then((response) => (response.ok ? response : Promise.reject(response)))
      .then((response) =>
        response.json().then((data) => {
          const geocodeResult = data.response.docs[0]
          if (geocodeResult) {
            const result = [
              'NL01',
              'Nederland',
              geocodeResult.provinciecode,
              geocodeResult.provincienaam,
              'GM' + geocodeResult.gemeentecode,
              geocodeResult.gemeentenaam,
              geocodeResult.wijkcode,
              geocodeResult.wijknaam,
              geocodeResult.buurtcode,
              geocodeResult.buurtnaam,
            ]
            // Set state nr
            const types = ['land', 'provincie', 'gemeente', 'wijk', 'buurt']
            const state = types.indexOf(geocodeResult.type)
            this.context.setAllNames(state, result)
            this.context.setActiveState(state)
          }
          this.clearList()
        })
      )
  }

  // Below functions deal with typing, selecting and submitting in the input bar
  onHandleClick = (item, e) => {
    e.preventDefault()
    this.setState(
      {
        value: {
          weergavenaam: item.weergavenaam || '',
        },
      },
      () => {
        this.lookupLocation(item)
      }
    )
    this.clearList()
  }

  handleSubmit = (e) => {
    // Prevent refresh on pressing Enter
    e.preventDefault()
  }

  handleKeyDown = (e) => {
    // Prevent default on just pressing enter
    if (e.key === 'Enter') e.preventDefault()
    // If the resultList exists
    if (this.state.resultList.length > 0) {
      switch (e.key) {
        case 'Enter':
          // Prevent refresh on pressing Enter
          e.preventDefault()
          if (this.state.cursorPosition >= 0) {
            const item = this.state.resultList[this.state.cursorPosition]

            this.setState(
              {
                value: {
                  weergavenaam: item.weergavenaam || '',
                },
              },
              () => {
                this.lookupLocation(item)
              }
            )
          }

          break
        case 'ArrowDown':
          // Until end of list
          if (this.state.resultList.length - 1 > this.state.cursorPosition) {
            this.setState((prevState) => ({
              cursorPosition: prevState.cursorPosition + 1,
            }))
          }
          break
        case 'ArrowUp':
          // Until beginning of list
          if (this.state.cursorPosition !== 0) {
            this.setState((prevState) => ({
              cursorPosition: prevState.cursorPosition - 1,
            }))
          }
          break

        case 'Backspace':
          this.clearList()
          break
        case 'Escape':
          this.clearList()
          break
        default:
          break
      }
    }
  }

  clearList = () => {
    this.setState({ resultList: [] })
    this.setState({ cursorPosition: -1 })
  }

  // Render component
  // For every possible result, create a LocationNavListItem
  render() {
    return (
      <ControlContainer>
        <input
          id="control-input"
          label="Zoek gemeente, wijk of buurt"
          placeholder="Zoek gemeente, wijk of buurt"
          type="text"
          value={this.state.value.weergavenaam}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
          onSubmit={this.handleSubmit}
          autoComplete="off"
          autoFocus="off"
        />

        {this.state.resultList.length ? (
          <ResultList>
            {this.state.resultList.map((item, i) => (
              <LocationNavListItem
                key={item.id}
                item={item}
                i={i}
                cursorPosition={this.state.cursorPosition}
                handleClick={this.onHandleClick}
              />
            ))}
          </ResultList>
        ) : null}
      </ControlContainer>
    )
  }
}

LocationNav.contextType = AppContext

export default LocationNav
