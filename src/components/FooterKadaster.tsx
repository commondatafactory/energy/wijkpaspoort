// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Drawer, Icon } from '@commonground/design-system'
import React, { useContext, useMemo, useState } from 'react'
import CsvDownloader from 'react-csv-downloader'

import dlImg from '../../public/img/download-2-fill.svg'
import linkImg from '../../public/img/link_v1.svg'
import { makeTableKadasterV2 } from '../utils/makeDownload'
import * as Styled from './Footer.Styles'
import { AppContext } from './geo-navigatie/GeoNavigatieContextAPI'

function FooterKadaster({ dataSet, regioCode, regio }) {
  // const [current, setCurrent] = useState({})
  const { activeAdminState, levelsAll } = useContext(AppContext)
  const [showDrawer, setShowDrawer] = useState(false)

  const current = useMemo(() => {
    if (activeAdminState === 2 || activeAdminState === 3) {
      return {
        admin: levelsAll[activeAdminState].admin,
        regio: regio,
        code: regioCode,
        naam: levelsAll[activeAdminState].filterNaam,
      }
    } else if (activeAdminState === 4) {
      return {
        admin: 'Buurt',
        regio: regio,
        code: regioCode,
        naam: levelsAll[3].highlightNaam,
      }
    } else {
      return {
        admin: '',
        regio: '',
        code: '',
        naam: '',
      }
    }
  }, [activeAdminState, levelsAll, regio, regioCode])

  const [completeDataSet, kadasterDataSet] = useMemo<
    { [key: string]: string }[][]
  >(() => {
    if (activeAdminState < 2 || !Object.keys(dataSet).length) {
      return [null, null]
    }

    return [
      [{ ...current, ...dataSet }],
      [makeTableKadasterV2(dataSet, activeAdminState, current)],
    ]
  }, [activeAdminState, dataSet, current])

  return (
    <Styled.FooterContainer>
      <Button
        onClick={() => setShowDrawer(true)}
        tabIndex="-1"
        name="download"
        variant="primary"
        disabled={activeAdminState < 2}
      >
        <Icon as={dlImg} size="small" inline />
        Download de Data als tabel
      </Button>

      <Button variant="secondary">
        <a
          title="Ga naar het Wijkpaspoort van het Kadaster"
          target="blank"
          href="https://www.kadaster.nl/zakelijk/producten/advies/wijkpaspoort-voor-de-warmtetransitie"
        >
          Wijkpaspoort Kadaster
          <Icon as={linkImg} size="small" inline />
        </a>
      </Button>

      {showDrawer && (
        <Drawer closeHandler={() => setShowDrawer(false)} autoFocus>
          <Drawer.Header title="Downloadservice" />
          <Drawer.Content className="drawercontent">
            <p>
              <b>
                <i>
                  Deze downloadservice helpt je om het complete Wijkpaspoort in
                  te vullen voor een wijk naar keuze. Je kunt hier het
                  kwantitatieve deel naar Excel exporteren via de onderstaande
                  downloadknop. Deze gegevens kun je vanuit Excel invullen in
                  het format van het Wijkpaspoort. Daarna kun je het
                  kwalitatieve deel zelf aanvullen.
                </i>
              </b>
            </p>
            <p>
              Alle relevante data van de bekeken wijken en buurten wordt
              ge-exporteerd. Wil je bijvoorbeeld de data van alle buurten uit
              een bepaalde wijk? Klik deze dan een voor een aan, en druk
              vervolgens op de download knop.
            </p>
            <p>
              <b>Stap 1.</b> Klik alle wijken en buurten een voor een aan om de
              data op te halen in de viewer.
            </p>
            <p>
              <b>Stap 2. </b> Heb je alle gewenste buurten en wijken aangeklikt?
              Druk dan op de gele downloadknop hieronder. Zo download je in een
              keer alle data van de wijken en buurten die je aangeklikt hebt.
            </p>
            <CsvDownloader
              datas={kadasterDataSet}
              filename={'wijkpaspoort_data_' + regioCode}
              text="Download"
            >
              <Button
                style={{ marginBottom: '0.75rem' }}
                onClick={() => {
                  setShowDrawer(true)
                }}
                name="download"
                variant="primary"
              >
                <Icon as={dlImg} size="small" inline />
                Tabel voor het Kadaster wijkpaspoort
              </Button>
            </CsvDownloader>
            <p>
              <b>Stap 3. </b> Sla het .csv bestand op.{' '}
            </p>
            <p>
              <b>Stap 4.</b> Importeer het .csv bestand eenvoudig in Excel. Weet
              je niet hoe? Kijk dan{' '}
              <a
                href="https://workshops.this-way.nl/workshop/dego/#csv-openen-in-microsoft-excel"
                target="blank"
              >
                {' '}
                op deze workshop <Icon as={linkImg} size="small" inline />
              </a>{' '}
            </p>

            <p>
              Voor verdere uitleg over het invullen van het complete
              wijkpaspoort zie:{' '}
              <a
                target="blank"
                href="https://www.kadaster.nl/zakelijk/producten/advies/wijkpaspoort-voor-de-warmtetransitie"
              >
                Wijkpaspoort Kadaster <Icon as={linkImg} size="small" inline />
              </a>
            </p>

            <p>{}</p>
            <p>{}</p>
            <p>
              <i>
                {' '}
                Met de knop hieronder kun je <b>alle</b> beschikbare gegevens
                (attributen) uit deze viewer downloaden. De attribuutnamen in de
                csv zijn hetzelfde als die bron, zodat je de gegevens
                makkelijker kunt herleiden tot de oorspronkelijke bron (bv
                Statline van CBS). Deze dataset is iets technischer dan de
                bovenstaande dataset.
              </i>
            </p>
            <CsvDownloader
              datas={completeDataSet}
              filename={'wijkpaspoort_data_' + regioCode}
              text="Download"
            >
              <Button
                onClick={() => setShowDrawer(true)}
                name="download"
                variant="secondary"
              >
                <Icon as={dlImg} size="small" inline />
                Tabel met alle data en CBS attribuut verwijzingen
              </Button>
            </CsvDownloader>
          </Drawer.Content>
        </Drawer>
      )}
    </Styled.FooterContainer>
  )
}
export default FooterKadaster
