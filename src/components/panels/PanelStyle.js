// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {} from '@commonground/design-system'
import styled from 'styled-components'

export const DataView = styled.div`
  margin-bottom: ${(d) => d.theme.tokens.spacing06};
  display: flex;
  flex-wrap: wrap;
  /* flex-direction:column; */
  :first-child {
    width: 100%;
  }

  @media screen and (max-width: 1824px) and (orientation: landscape) {
    margin-bottom: 20px;
  }

  /* CONTENT */

  .graphTitle {
    margin-top: 15px;
    margin-bottom: 5px;
    width: 100%;
  }

  /* CONTENT */

  .graphNote {
    margin-top: 0px;
    margin-bottom: 15px;
    padding: 0;
    font-weight: normal;
    display: block;
    text-align: center;
  }

  div.legendaCircle {
    width: 20px;
    height: 20px;
    border-radius: 10px;
    margin: 5px;
  }
  div.legendItem {
    display: flex;
  }
`

export const UtiliteitDiv = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: flex-end;
  align-items: stretch;
  align-content: space-between;
  height: 200px;

  .utiliteitItem {
    display: flex;
    padding: 4px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    /* flex-wrap: wrap; */
    width: 50%;
  }

  .utiliteitItem img {
    margin-right: 15px;
  }
`

export const EnergieArmoedeTitle = styled.div`
  width: 100%;
  font-size: 10pt;
  display: flex;
  justify-content: space-between;
  margin: 0;
  margin-left: 90px;

  p {
    margin: 0;
    font-size: 0.75rem;
    color: var(--colorPaletteGray700);
  }
`
