// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Collapsible } from '@commonground/design-system'
import { number, object, string } from 'prop-types'
import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

import Source from './asset/SourceDescription'

const StyledPanel = styled.div`
  padding: ${(div) => div.theme.tokens.spacing05};
  background-color: #ffffff;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16);
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  border-radius: 4px;

  /* Collapsible look */

  .indexstyles__CollapsibleButton-sc-1udtzwt-1 {
    text-align: center;
    text-decoration-line: underline;
    color: #0b71a1;
    border-top: #bdbdbd 1px solid;
    padding: 0;
    margin: 0;
  }

  /* BronBeschrijving */

  .indexstyles__CollapsibleButton-sc-1udtzwt-1,
  .indexstyles__CollapsibleTitle-sc-1udtzwt-2 {
    text-decoration: none !important;
    font-size: 0.75rem;
    margin: 0;
    cursor: pointer;
    color: var(--colorPaletteGray700);
    text-decoration-line: none !important;
    text-decoration-style: none;
    padding-top: 10px;
  }
`

const StyledPanelTitle = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  margin-bottom: ${(div) => div.theme.tokens.spacing05};

  h2 {
    margin: 0px;
    margin-bottom: 0px;
    margin-left: 3px;
    font-size: 24px;
  }

  .icon {
    width: 24px;
    height: 24px;
    padding-right: 4px;
    color: #9e9e9e;
  }

  .icon:focus {
    outline: 2px var(--colorInfo) solid;
  }
`

function Panel({
  title,
  thema,
  currentActiveAdminCode,
  dataSet,
  icon,
  children,
}) {
  const [isActive, toggleActive] = useState(true)
  const itemRef = useRef(null)
  const [itemWidth, setItemWidth] = useState(-1)
  const [content, setContent] = useState()

  // holds the timer for setTimeout and clearInterval
  let MOVEMENT_TIMER = null

  // the number of ms the window size must stay the same size before the
  // dimension state variable is reset
  const RESET_TIMEOUT = 50

  const TEST_DIMENSIONS = () => {
    // For some reason targetRef.current.getBoundingClientRect was not available
    // I found this worked for me, but unfortunately I can't find the
    // documentation to explain this experience
    if (itemRef.current) {
      setItemWidth(itemRef.current.offsetWidth - 30)
    }
  }

  // This sets the dimensions on the first render
  useEffect(() => {
    TEST_DIMENSIONS()

    // every time the window is resized, the timer is cleared and set again
    // the net effect is the component will only reset after the window size
    // is at rest for the duration set in RESET_TIMEOUT.  This prevents rapid
    // redrawing of the component for more complex components such as charts
    window.addEventListener('resize', () => {
      clearInterval(MOVEMENT_TIMER)
      MOVEMENT_TIMER = setTimeout(TEST_DIMENSIONS, RESET_TIMEOUT)
    })
  }, [])

  // update on dataSet change
  useEffect(() => {
    if (dataSet) {
      setItemWidth(Math.floor(itemRef.current.offsetWidth) - 31)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet, isActive])

  // update on width change
  useEffect(() => {
    if (dataSet) {
      const childrenWithProps = React.Children.map(children, (child) => {
        // Checking isValidElement is the safe way and avoids a
        // typescript error too.
        if (React.isValidElement(child)) {
          return React.cloneElement(child, { width: itemWidth })
        }
      })
      children.bind
      setContent(childrenWithProps)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemWidth, dataSet])

  return (
    <StyledPanel
      role="listitem"
      ref={itemRef}
      className={isActive ? 'Panel' : 'PanelSmall'}
    >
      <StyledPanelTitle>
        <img
          width="36"
          height="36"
          className="icon"
          alt={'Samenvatting ' + thema + ' icon'}
          src={icon}
          onClick={() => toggleActive(!isActive)}
        />
        <h2>{title}</h2>
      </StyledPanelTitle>
      {isActive && content}
      {isActive && (
        <Collapsible title="Bron beschrijving" alt="Fold out Bron beschrijving">
          <Source thema={thema} />
        </Collapsible>
      )}
    </StyledPanel>
  )
}
Panel.propTypes = {
  width: number,
  title: string,
  thema: string,
  regioCode: string,
  regio: string,
  dataSet: object,
}
export default Panel
