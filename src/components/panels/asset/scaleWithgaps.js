// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export default function scaleWithGaps(scale, where, gapSize) {
  scale = scale.copy()
  var offsets = {}
  var i = 0
  var offset = -(scale.step() * gapSize * where.length) / 2
  scale.domain().forEach((d, j) => {
    if (j == where[i]) {
      offset += scale.step() * gapSize
      ++i
    }
    offsets[d] = offset
  })
  var newScale = (value) => scale(value) + offsets[value]
  // Give the new scale the methods of the original scale
  for (var key in scale) {
    newScale[key] = scale[key]
  }
  newScale.copy = function () {
    return scaleWithGaps(scale, where, gapSize)
  }
  return newScale
}
