// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'
import { array, number, object } from 'prop-types'
import React, { useEffect, useRef } from 'react'

import { wrapVert } from '../../../utils/helper'

function Graph({ dataSet, colors, margin, width, height, average, time }) {
  const graph = useRef(null)
  margin = margin || { top: 0, right: 0, bottom: 0, left: 30 }
  width = width - margin.left - margin.right || 350 - margin.left - margin.right
  height =
    height - margin.top - margin.bottom || 120 - margin.top - margin.bottom
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']
  d3.formatDefaultLocale({
    thousands: '.',
    grouping: [3],
    currency: ['', '€'],
  })

  useEffect(() => {
    // inital setting up graph
    d3.select(graph.current)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${width + margin.left + margin.right} ${
          height + margin.top + margin.bottom
        }`
      )

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    // Update graph
    if (graph.current && dataSet) {
      const max =
        d3.max(Object.entries(dataSet).map(([_, { value }]) => value)) * 1.1 + 1
      const x = d3
        .scaleBand()
        .range([0, width])
        .domain(dataSet.map((el) => el.key))
        .padding(0.1)
      const y = d3.scaleLinear().range([height, 0]).domain([0, max])
      const colorscale = d3
        .scaleOrdinal()
        .domain(dataSet.map((el) => el.key))
        .range(colors)

      const svg = d3.select(graph.current)

      y.domain([0, max])

      const yAxis = d3
        .axisLeft(y)
        .ticks(5)
        .tickFormat((d) => {
          if (d / 10000 >= 1) {
            d = d / 1000 + 'K'
          }
          return d
        })

      svg
        .selectAll('g.y.axis')
        .data([max])
        .join(
          (enter) =>
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .classed('y axis', true)
              .call(yAxis)
              .call((enter) => enter.transition().duration(time).call(yAxis)),
          (update) =>
            update.call((update) =>
              update.transition().duration(time).call(yAxis)
            )
        )
      const xAxis = d3.axisBottom(x)

      // Keep one axis
      svg
        .selectAll('g.x.axis')
        .data([1])
        .join((enter) =>
          enter
            .append('g')
            .attr(
              'transform',
              `translate(${margin.left},${height + margin.top})`
            )
            .classed('x axis', true)
            .call(xAxis)
            .selectAll('.tick text')
            .attr('transform', (d) => {
              if (d.length >= 2) {
                return 'translate(-10,10)rotate(-60)'
              } else {
                return 'translate(0,0)rotate(0)'
              }
            })
            .style('text-anchor', (d) => {
              if (d.length >= 2) {
                return 'end'
              }
            })
            .call(wrapVert, margin.bottom)
        )
      svg
        .selectAll('rect')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .attr('class', 'bar')
              .attr('transform', `translate(${margin.left} , ${margin.top})`)
              .attr('width', x.bandwidth())
              .attr('x', (d) => {
                return x(d.key)
              })
              .style('fill', (d) => {
                return colorscale(d.key)
              })
              .attr('y', (d) => {
                return y(d.value)
              })
              .attr('height', (d) => {
                return height - y(d.value)
              })
              .call((enter) =>
                enter
                  .transition()
                  .duration(time)
                  .attr('y', (d) => {
                    return y(d.value)
                  })
                  .attr('height', (d) => {
                    return height - y(d.value)
                  })
              ),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('y', (d) => {
                  return y(d.value)
                })
                .attr('height', (d) => {
                  return height - y(d.value)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition()
                .duration(time)
                .attr('y', height)
                .attr('height', 0)
                .remove()
            )
        )
      svg
        .selectAll('.label')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('label', true)
              .attr('transform', `translate(${margin.left}, 0)`)
              .attr('text-anchor', 'middle')
              .style('fill', '#757575')
              .attr('dy', '3px')
              .attr('y', function (d) {
                return y(d.value)
              })
              .attr('x', function (d) {
                return x(d.key) + x.bandwidth() / 2
              })
              .text((d) => {
                return d3.format(',')(d.value)
              }),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)

                .attr('y', function (d) {
                  return y(d.value)
                })
                .text((d) => {
                  return d3.format(',')(d.value)
                })
            ),
          (exit) =>
            exit.call((exit) => exit.transition().duration(time).remove())
        )
      if (average) {
        svg
          .selectAll('line.average')
          .data([average])
          .join(
            (enter) =>
              enter
                .append('line')
                .classed('average', true)
                .attr('transform', `translate(${margin.left} , ${margin.top})`)
                .attr('x1', x(0))
                .style('stroke', '#7794A1')
                .style('stroke-dasharray', '3,3')
                .style('stroke-width', 2)
                .attr('x2', width)
                .attr('y1', y(average))
                .attr('y2', y(average)),
            (update) =>
              update.call((update) =>
                update
                  .transition()
                  .duration(time)
                  .attr('y1', y(average))
                  .attr('y2', y(average))
              ),
            (exit) =>
              exit.call((exit) => exit.transition().duration(time).remove())
          )
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet])

  return <svg ref={(el) => (graph.current = el)} />
}

Graph.propTypes = {
  dataSet: array,
  colors: array,
  margin: object,
  width: number,
  height: number,
  average: number,
}

export default Graph
