// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'
import { array, number, object, string } from 'prop-types'
import React, { useEffect, useRef } from 'react'

function Graph({ dataSet, colors, width, height, margin, graphTitle, labels }) {
  const graph = useRef(null)

  margin = margin || { top: 0, right: 0, bottom: 0, left: 0 }
  width = width - margin.left - margin.right || 300 - margin.left - margin.right
  height =
    height - margin.top - margin.bottom || 150 - margin.top - margin.bottom
  colors = colors || ['#7794A1', '#0ba19f']

  d3.formatDefaultLocale({
    thousands: '.',
    grouping: [3],
    currency: ['', '€'],
  })

  useEffect(() => {
    // inital setting up graph
    d3.select(graph.current)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('title', graphTitle)
      .attr('alt', graphTitle)
      .attr('name', graphTitle)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${width + margin.left + margin.right} ${
          height + margin.top + margin.bottom
        }`
      )

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    // Update graph
    if (dataSet && graph.current) {
      // MAX Left
      const maxL =
        d3.max(Object.entries(dataSet).map(([_, { value }]) => value)) + 1
      // MAX Right
      const maxR =
        d3.max(Object.entries(dataSet).map(([_, { value2 }]) => value2)) + 1
      const max = maxR > maxL ? maxR : maxL
      const xL = d3
        .scaleLinear()
        .range([0, width / 2])
        .domain([0, max])

      const xR = d3
        .scaleLinear()
        .range([0, width / 2])
        .domain([0, max])

      const y = d3
        .scaleBand()
        .range([0, height])
        .domain(dataSet.map((el) => el.key))
        .padding(0.1)

      const svg = d3.select(graph.current)
      const t = svg.transition().duration(750)

      const yAxis = d3.axisLeft(y)

      // LEFT GROUP Keep one axis
      svg
        .selectAll('g.y.axis')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .classed('y axis horz', true)
              .call(yAxis)
              .call((enter) => enter.transition(t).call(yAxis))
            enter.selectAll('.tick line').remove()
          },
          (update) => update.call((update) => update.transition(t).call(yAxis))
        )

      // Titels
      svg
        .selectAll('g.titleLabel')
        .data([1])
        .join((enter) => {
          enter
            .append('g')
            .classed('titleLabel', true)
            .attr('transform', `translate(${margin.left},  ${margin.top})`)
            .call((g) =>
              g
                .append('text')
                .attr('x', width / 2 - xL(0) - 5)
                .attr('y', -3)
                .style('fill', '#0B71A1')
                .attr('text-anchor', 'end')
                .text('← ' + labels[0])
            )
            .call((g) =>
              g
                .append('text')
                .attr('x', width / 2 + 5)
                .attr('y', -3)
                .style('fill', '#7794A1')
                .attr('text-anchor', 'start')
                .text(labels[1] + ' →')
            )
        })
      // RIGHT GROUP
      svg
        .selectAll('g.graph')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .classed('graph', true)
          }
          // (update) => update.call((update) => update.transition(t).attr("x", width / 2 - xL(0) - 5)
          //   .attr("y", 0))
        )
      svg
        .select('g.graph')
        .selectAll('rect.left')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .attr('class', 'left')
              // .attr('transform', `translate(${margin.left},  ${margin.top})`)
              .attr('x', (d) => {
                return width / 2 - xL(d.value)
              })
              .attr('y', (d) => {
                return y(d.key)
              })
              .attr('height', y.bandwidth())
              .attr('width', function (d) {
                return xL(d.value)
              })
              .style('fill', colors[0]),
          (update) =>
            update.call((update) =>
              update
                .transition(t)
                .attr('width', function (d) {
                  return xL(d.value)
                })
                .attr('x', (d) => {
                  return width / 2 - xL(d.value)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition(t)
                .attr('width', xL(0))
                .attr('x', width / 2 - xL(0))
                .remove()
            )
        )

      svg
        .select('g.graph')
        .selectAll('rect.right')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .classed('right', true)
              // .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .attr('x', () => {
                return width / 2
              })
              .attr('y', (d) => {
                return y(d.key)
              })
              .attr('width', function (d) {
                return xR(d.value2)
              })
              .attr('height', y.bandwidth())
              .style('fill', colors[1]),
          (update) =>
            update.call((update) =>
              update
                .transition(t)
                .attr('x', () => {
                  return width / 2
                })
                .attr('width', function (d) {
                  return xR(d.value2)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition(t)
                .attr('x', () => {
                  return width / 2
                })
                .attr('width', xR(0))
                .remove()
            )
        )
      // labels left
      svg
        .select('g.graph')
        .selectAll('.labelLeft')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('labelLeft', true)
              // .attr('transform', `translate(${margin.left},  ${margin.top})`)
              .attr('text-anchor', 'left')
              .style('fill', function (d) {
                if (d.value < max / (Object.keys(dataSet).length / 2)) {
                  return '#757575'
                } else {
                  return '#f5f5f5'
                }
              })
              .attr('dy', '4px')
              .attr('x', function (d) {
                if (d.value < max / (Object.keys(dataSet).length / 2)) {
                  return width / 2 - xL(d.value) - d.value.toString().length * 8
                } else {
                  return width / 2 - xL(d.value) + 5
                }
              })
              .attr('y', function (d) {
                return y(d.key) + y.bandwidth() / 2
              })
              .text((d) => {
                if (d.value.toString().length > 2) {
                  return d3.format(',')(d.value)
                } else {
                  return d.value
                }
              }),
          (update) =>
            update.call((update) =>
              update
                .transition(t)
                .style('fill', function (d) {
                  if (d.value < max / (Object.keys(dataSet).length / 2)) {
                    return '#757575'
                  } else {
                    return '#f5f5f5'
                  }
                })
                .attr('x', function (d) {
                  if (d.value < max / (Object.keys(dataSet).length / 2)) {
                    return (
                      width / 2 - xL(d.value) - d.value.toString().length * 8
                    )
                  } else {
                    return width / 2 - xL(d.value) + 5
                  }
                })
                .text((d) => {
                  if (d.value.toString().length > 2) {
                    return d3.format(',')(d.value)
                  } else {
                    return d.value
                  }
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition(t)
                .attr('x', width / 2 - xL(0) - 8)
                .remove()
            )
        )
      // labels right
      svg
        .select('g.graph')

        .selectAll('.labelRight')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('labelRight', true)
              // .attr('transform', `translate(${margin.left},  ${margin.top})`)
              .attr('text-anchor', 'right')
              .style('fill', function (d) {
                if (d.value2 < max / (Object.keys(dataSet).length / 2)) {
                  return '#757575'
                } else {
                  return '#f5f5f5'
                }
              })
              .attr('dy', '4px')
              .attr('x', function (d) {
                if (d.value2 < max / (Object.keys(dataSet).length / 2)) {
                  return width / 2 + xR(d.value2) + 5
                } else {
                  return (
                    width / 2 + xR(d.value2) - d.value2.toString().length * 8
                  )
                }
              })
              .attr('y', function (d) {
                return y(d.key) + y.bandwidth() / 2
              })
              .text((d) => {
                if (d.value2.toString().length > 2) {
                  return d3.format(',')(d.value2)
                } else {
                  return d.value2
                }
              }),
          (update) =>
            update.call((update) =>
              update
                .transition(t)
                .style('fill', function (d) {
                  if (d.value2 < max / (Object.keys(dataSet).length / 2)) {
                    return '#757575'
                  } else {
                    return '#f5f5f5'
                  }
                })
                .attr('x', function (d) {
                  if (d.value2 < max / (Object.keys(dataSet).length / 2)) {
                    return width / 2 + xR(d.value2) + 5
                  } else {
                    return (
                      width / 2 + xR(d.value2) - d.value2.toString().length * 8
                    )
                  }
                })
                .text((d) => {
                  if (d.value2.toString().length > 2) {
                    return d3.format(',')(d.value2)
                  } else {
                    return d.value2
                  }
                })
            ),
          (exit) => exit.call((exit) => exit.transition(t).remove())
        )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet])

  return <svg ref={graph} style={{ width: '100%' }} />
}

Graph.propTypes = {
  dataSet: array,
  colors: array,
  margin: object,
  width: number,
  height: number,
  graphTitle: string,
}

export default Graph
