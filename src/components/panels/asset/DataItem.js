// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, oneOfType, string } from 'prop-types'
import React from 'react'
import styled from 'styled-components'

import { decide } from '../../../utils/helper'

const StyledDataItem = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: baseline;
  flex-wrap: nowrap;
  margin: 0px;
  h3 {
    margin: ${(h3) => h3.theme.tokens.spacing03};
    margin-left: 0px;
    margin-bottom: 0px;
    font-size: 1.4em;
    color: #0b71a1;
  }

  p {
    margin: 0;
    padding: 0;
  }
`
function DataItem({ label, value1, value2, unit }) {
  return (
    <StyledDataItem>
      <h3>{decide(value1, value2, unit)}</h3>
      <p>{label}</p>
    </StyledDataItem>
  )
}

DataItem.propTypes = {
  value1: oneOfType([number, string]),
  value2: oneOfType([number, string]),
  unit: string,
  label: string,
}

export default DataItem
