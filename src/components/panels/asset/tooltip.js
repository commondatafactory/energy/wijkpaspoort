// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import './tooltip.css'

import * as d3 from 'd3'
import { number, string } from 'prop-types'
import React, { useEffect, useRef } from 'react'

function Tooltip({ content, x, y }) {
  const tooltipRef = useRef(null)

  useEffect(() => {
    if (tooltipRef.current) {
      d3.select(tooltipRef.current).attr('x', x)
      d3.select(tooltipRef.current).attr('y', y)
    }
  }, [x, y])

  return (
    <text className="tooltip" ref={(el) => (tooltipRef.current = el)}>
      {content}
    </text>
  )
}

Tooltip.propTypes = {
  content: string,
  x: number,
  y: number,
}

export default Tooltip
