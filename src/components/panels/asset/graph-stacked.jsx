// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

/* eslint-disable no-inner-declarations */
import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
import { wrapVert } from '../../../utils/helper'

function Graph({
  dataSet,
  colors,
  time,
  margin,
  height,
  width,
  subgroups,
  groups,
}) {
  const graph = useRef(null)
  const div = useRef(null)

  margin = margin || { top: 0, right: 0, bottom: 0, left: 150 }
  height =
    height - margin.top - margin.bottom || 350 - margin.top - margin.bottom
  width = width - margin.left - margin.right || 350 - margin.left - margin.right
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']

  useEffect(() => {
    // inital setting up graph
    d3.select(graph.current)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${width + margin.left + margin.right} ${
          height + margin.top + margin.bottom
        }`
      )
  }, [width, height])

  useEffect(() => {
    if (dataSet && graph.current) {
      const svg = d3.select(graph.current)

      // kleur
      const colorScale = d3.scaleOrdinal().domain(subgroups).range(colors)

      // copy dataset without totaal
      const copyDataset = [...dataSet].map((obj) => {
        let newObject = (({ totaal, ...object }) => object)(obj)
        return newObject
      })
      // stack data
      let stackedData = d3.stack().keys(subgroups)(copyDataset)
      stackedData.forEach((stackedBar) => {
        stackedBar.forEach((stack) => {
          stack.id = `${stackedBar.key}`
        })
      })

      const max = d3.max(stackedData[stackedData.length - 1], (d) => d[1])
      if (max === 0) {
        svg.selectAll().remove()
      }
      // y linear scale
      const y = d3
        .scaleLinear()
        .range([height, 0])
        .domain([0, max + max / 100])
      const yAxis = d3.axisLeft(y)

      //  x categories
      const x = d3
        .scaleBand()
        .range([0, width - margin.right - margin.left])
        .domain(groups)
        .padding(0.1)

      // Y- axis
      svg
        .selectAll('g.y.axis')
        .data([1])
        .join(
          (enter) =>
            enter
              .append('g')
              .classed('y axis', true)
              .call(yAxis)
              .attr('transform', `translate(${margin.left}, 0)`),
          (update) => update.transition().duration(time).call(yAxis)
        )

      // x axis
      svg
        .selectAll('g.x.axis')
        .data([1])
        .join(
          (enter) =>
            enter
              .append('g')
              .attr('transform', `translate(${margin.left},${height})`)
              .classed('x axis', true)
              .call(d3.axisBottom(x))
              .selectAll('.tick text')
              .attr('transform', (d) => {
                if (d.length >= 2) {
                  return 'translate(-10,10)rotate(-60)'
                } else {
                  return 'translate(0,0)rotate(0)'
                }
              })
              .style('text-anchor', (d) => {
                if (d.length >= 2) {
                  return 'end'
                }
              })
              .call(wrapVert, 40),
          (update) => update.transition().duration(time).call(d3.axisBottom(x))
        )

      function updateRects(childRects) {
        childRects
          .selectAll('g')
          .data(
            (d) => d,
            (d) => d.id
          )
          .join(
            (enter) => {
              let g = enter
                .append('g')
                .attr('class', 'data-group')
                .on('mouseover', function (d) {
                  d3.select(this.parentNode.parentNode).raise()
                  d3.select(this)
                    .select('rect')
                    .style('stroke', '#000000')
                    .style('cursor', 'pointer')
                  d3.select(this)
                    .select('text')
                    .style('font-size', '20px')
                    .style('font-weight', 'bold')
                    .style('stroke', '#ffffff')
                    .style('stroke-width', 1)
                    .style('stroke-opacity', 0.5)
                    .style('fill', '#000000')
                })
                .on('mouseout', function () {
                  d3.select(this).select('rect').style('stroke', '')
                  d3.select(this)
                    .select('text')
                    .style('font-size', '10px')
                    .style('fill', '#ffffff')
                    .style('font-weight', 'normal')
                    .style('stroke-width', 0)
                    .style('stroke-opacity', 0)
                })

              g.append('rect')
                .attr('id', (d) => d.id)
                .attr('class', 'bar')
                .attr('y', (d) => y(d[1]))
                .attr('height', (d) => y(d[0]) - y(d[1]))
                .attr('width', x.bandwidth())
                .attr('x', (d) => x(d.data.key))

              g.append('text')
                .text((d) => {
                  if (d.data[d.id] !== 0) {
                    return d.data[d.id]
                  }
                })
                .attr('text-anchor', 'middle')
                .attr('y', (d) => y(d[1]) + (y(d[0]) - y(d[1])) / 1.5)
                .attr('x', (d) => x(d.data.key) + x.bandwidth() / 2)
                .style('font', '10px sans-serif')
                .style('fill', '#ffffff')
              return g
            },
            (update) => {
              update
                .select('rect')
                .transition()
                .duration(time)
                .attr('y', (d) => y(d[1]))
                .attr('dy', '.35em')
                .attr('height', (d) => y(d[0]) - y(d[1]))
                .attr('width', x.bandwidth())
                .attr('x', (d) => x(d.data.key))

              update
                .select('text')
                .transition()
                .duration(time)
                .text((d) => {
                  if (d.data[d.id] !== 0) {
                    return d.data[d.id]
                  }
                })
                .attr('y', (d) => y(d[1]) + (y(d[0]) - y(d[1])) / 1.5)
                .attr('x', (d) => x(d.data.key) + x.bandwidth() / 2)
              return update
            },
            (exit) => exit.transition().remove()
          )
      }

      // Show the bars
      let bars = svg.selectAll('g.stack').data(stackedData, (d) => {
        return d.key
      })

      bars.join(
        (enter) => {
          const barEnter = enter
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',0)')
            .attr('class', 'stack')

          barEnter
            .append('g')
            .attr('class', 'bars')
            .attr('fill', (d, i) => colorScale(i))

          updateRects(barEnter.select('.bars'))
          return enter
        },
        (update) => {
          updateRects(update.select('.bars'))
        },
        (exit) => {
          return exit.remove()
        }
      )
    }
  }, [dataSet, width])

  return (
    <div ref={(el) => (div.current = el)}>
      <svg ref={(el) => (graph.current = el)} />{' '}
    </div>
  )
}

export default Graph
