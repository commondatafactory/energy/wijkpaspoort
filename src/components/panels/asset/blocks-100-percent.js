// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'
import { array, number, object } from 'prop-types'
import React, { useEffect, useRef, useState } from 'react'

function Blocks({ dataSet, width, height, margin, title }) {
  const divBlocks = useRef(null)
  const [total, setTotal] = useState()
  margin = margin || { top: 0, right: 0, bottom: 0, left: 0 }
  width = Math.floor(width)
  height = Math.floor(height)

  useEffect(() => {
    if (dataSet && divBlocks.current) {
      const mainDiv = d3.select(divBlocks.current)

      mainDiv
        .style('width', width + 'px')
        .style('height', 'auto')
        .style('display', 'flex')
        .style('flex-wrap', 'wrap')

      const xScale = d3
        .scaleLinear()
        //accepts
        .domain([0, 100])
        //outputs
        .range([0, width])

      let domainRange = []
      for (const el in dataSet) {
        if (Object.hasOwnProperty.call(dataSet, el)) {
          const value = parseInt(dataSet[el].value)
          const lastValue = domainRange[domainRange.length - 1] || 0
          domainRange.push(lastValue + value)
        }
      }

      // Range of values
      const newDataSet = d3.range(
        Math.max(domainRange[domainRange.length - 1], 100)
      )
      const newColors = dataSet.map((el) => el.color)

      setTotal(domainRange[domainRange.length - 1])
      if (domainRange[domainRange.length - 1] < 100) {
        domainRange.push(100)
        newColors.push('#ffffff')
      }
      const scaleThreshold = d3
        .scaleThreshold()
        .domain(domainRange)
        .range(newColors)

      mainDiv
        .selectAll('div')
        .data(newDataSet)
        .join(
          (enter) => {
            enter
              .append('div')
              .classed('blockvalues', true)
              .attr('id', (d) => d + 'block')
              .style('background', (d) => {
                return scaleThreshold(d)
              })
              .style('border', (d) => {
                return 'solid 1px ' + d3.color(scaleThreshold(d)).darker(0.7)
              })
              .style('height', (d) => Math.floor(xScale(10)) + 'px')
              .style('width', (d) => Math.floor(xScale(10)) + 'px')
          },
          (update) =>
            update.call((update) =>
              update
                .transition()
                .style('background', (d) => {
                  return scaleThreshold(d)
                })
                .style('border', (d) => {
                  return 'solid 1px ' + d3.color(scaleThreshold(d)).darker(0.7)
                })
                .style('height', (d) => Math.floor(xScale(10)) + 'px')
                .style('width', (d) => Math.floor(xScale(10)) + 'px')
            ),
          (exit) =>
            exit.call((exit) =>
              exit.transition().style('background', '#ffffff').remove()
            )
        )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet, width, height, margin])

  return (
    <>
      <ul>
        {dataSet
          ? dataSet.map((element, i) => (
              <div key={element.key + 'legend' + i} className="legendItem">
                <div
                  key={element.key + 'block' + i}
                  className="legendBlock"
                  style={{
                    backgroundColor: element.color,
                    width: width / 7 + 'px',
                    height: width / 7 + 'px',
                  }}
                >
                  <p key={element.key + 'value' + i} style={{ color: 'white' }}>
                    {element.value}%
                  </p>
                </div>
                <p
                  style={{ color: d3.color(element.color).darker(1) }}
                  key={element.key + 'color' + i}
                >
                  {' '}
                  {element.title}{' '}
                </p>
              </div>
            ))
          : ''}
      </ul>
      <h3>{title}</h3>
      <div className="blocksGraph" ref={(el) => (divBlocks.current = el)} />
      <div className="blocksLegend">
        <div
          style={{
            border: 'solid 1px #616161',
            width: width / 10 + 'px',
            height: width / 10 + 'px',
          }}
        />
        <p>=1%</p>
        {total !== 100 && (
          <p style={{ fontSize: '0.7rem' }}>
            Door afronding kan het totaal meer of minder dan 100% zijn
          </p>
        )}
      </div>
    </>
  )
}
Blocks.propTypes = {
  dataSet: array,
  colors: array,
  margin: object,
  width: number,
  height: number,
}

export default Blocks
