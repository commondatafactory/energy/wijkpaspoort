// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { string } from 'prop-types'
import React from 'react'
import styled from 'styled-components'

import sources from '../../../data/bron_beschrijving'

const StyledSourceItem = styled.div`
  margin-top: 10px;
  margin-bottom: 25px;
  text-align: justify;

  p,
  h5 {
    margin: 0;
  }
`

function Source({ thema }) {
  // eslint-disable-next-line
  const info = sources[thema]

  return (
    <div>
      {Object.entries(info).map(([key, item]) => (
        <StyledSourceItem key={key}>
          <h5>
            {' '}
            <a target="blank" href={item.url}>
              {item.title}
            </a>
          </h5>
          <p dangerouslySetInnerHTML={{ __html: item.beschrijving }} />
          <p> {item.aggregatie} </p>
          <p> Data van {item.jaar} </p>
        </StyledSourceItem>
      ))}
    </div>
  )
}
export default Source

Source.propTypes = {
  thema: string,
}
