// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'
import { array, number, object } from 'prop-types'
import React, { useEffect, useRef } from 'react'

import { wrapHorz } from '../../../utils/helper'

function Graph({ dataSet, colors, margin, width, height, time }) {
  const graph = useRef(null)

  margin = margin || { top: 0, right: 0, bottom: 0, left: 0 }
  width = width - margin.left - margin.right || 300 - margin.left - margin.right
  height =
    height - margin.top - margin.bottom || 150 - margin.top - margin.bottom
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']
  d3.formatDefaultLocale({
    thousands: '.',
    grouping: [3],
    currency: ['', '€'],
  })

  useEffect(() => {
    // inital setting up graph
    d3.select(graph.current)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${width + margin.left + margin.right} ${
          height + margin.top + margin.bottom
        }`
      )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [width, height])

  useEffect(() => {
    // Update graph
    if (dataSet && graph.current) {
      const max = d3.max(Object.entries(dataSet).map(([_, { value }]) => value))
      const x = d3.scaleLinear().range([0, width]).domain([0, max])
      const y = d3
        .scaleBand()
        .range([0, height])
        .domain(dataSet.map((el) => el.key))
        .padding(0.1)

      const svg = d3.select(graph.current)

      const yAxis = d3.axisLeft(y)

      // Keep one axis
      svg
        .selectAll('g.y.axis')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, 0)`)
              .classed('y axis horz', true)
              .call(yAxis)
              .selectAll('.tick text')
              .style('text-anchor', (d) => {
                if (d.length >= 2) {
                  return 'end'
                }
              })
              .call(wrapHorz, margin.left - 5)
          },
          (update) => {
            update
              .call(yAxis)

              .selectAll('.tick text')
              .style('text-anchor', (d) => {
                if (d.length >= 2) {
                  return 'end'
                }
              })
              .call(wrapHorz, margin.left - 5)
          }
        )

      svg
        .selectAll('rect')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .attr('transform', `translate(${margin.left}, 0)`)
              .attr('height', y.bandwidth())
              .attr('x', () => {
                return y(0)
              })
              .attr('y', (d) => {
                return y(d.key)
              })
              .style('fill', colors[0])
              .attr('width', 0)
              .call((enter) => enter.transition().duration(time))
              .attr('width', function (d) {
                return x(d.value)
              }),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('width', function (d) {
                  return x(d.value)
                })
                .attr('height', y.bandwidth())
                .attr('y', (d) => {
                  return y(d.key)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit.transition().duration(time).attr('width', 0).remove()
            )
        )
      svg
        .selectAll('.label')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('label', true)
              .attr('transform', `translate(${margin.left}, 0)`)
              .attr('text-anchor', 'right')
              .style('fill', function (d) {
                if (d.value < max / (Object.keys(dataSet).length / 2)) {
                  return '#757575'
                } else {
                  return '#f5f5f5'
                }
              })
              .attr('dy', '4px')
              .attr('x', function (d) {
                if (d.value < max / (Object.keys(dataSet).length / 2)) {
                  return x(d.value) + 5
                } else {
                  return x(d.value) - d.value.toString().length * 7
                }
              })
              .attr('y', function (d) {
                return y(d.key) + y.bandwidth() / 2
              })
              .text((d) => {
                return d3.format(',')(d.value)
              }),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .style('fill', function (d) {
                  if (d.value < max / (Object.keys(dataSet).length / 2)) {
                    return '#757575'
                  } else {
                    return '#f5f5f5'
                  }
                })
                .attr('x', function (d) {
                  if (d.value < max / (Object.keys(dataSet).length / 2)) {
                    return x(d.value) + 5
                  } else {
                    return x(d.value) - d.value.toString().length * 7
                  }
                })
                .attr('y', function (d) {
                  return y(d.key) + y.bandwidth() / 2
                })
                .text((d) => {
                  return d3.format(',')(d.value)
                })
            ),
          (exit) =>
            exit.call((exit) => exit.transition().duration(time).remove())
        )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet, width, height])

  return <svg ref={(el) => (graph.current = el)} />
}

Graph.propTypes = {
  dataSet: array,
  colors: array,
  margin: object,
  width: number,
  height: number,
  time: number,
}

export default Graph
