// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, object } from 'prop-types'
import React, { useMemo } from 'react'

import DataItem from '../asset/DataItem'
import { DataView } from '../PanelStyle'
import Grafiek from './../asset/bar-div-100-perc'
import Graph from './../asset/graph-horizontal'

function PanelS({ dataSet, width }) {
  const [InwonersData, HuishoudenData, InkomensNiveauData] = useMemo(() => {
    // if (!dataSet || Object.values(dataSet).length == 0) {
    //   return [[], [], []]
    // }
    let inwoners = []
    let huishoudens = []
    let inkomensNiveau = []

    if ('k_0Tot15Jaar' in dataSet) {
      inwoners = [
        { key: '0 tot 15 jaar', value: dataSet.k_0Tot15Jaar || 0 },
        { key: '15 tot 25 jaar', value: dataSet.k_15Tot25Jaar || 0 },
        { key: '25 tot 45 jaar', value: dataSet.k_25Tot45Jaar || 0 },
        { key: '45 tot 65 jaar', value: dataSet.k_45Tot65Jaar || 0 },
        { key: '65 of ouder', value: dataSet.k_65JaarOfOuder || 0 },
      ]
    }

    if ('Eenpersoonshuishoudens' in dataSet) {
      huishoudens = [
        {
          key: 'Eenpersoons',
          value: dataSet.Eenpersoonshuishoudens || 0,
        },
        {
          key: 'Zonder Kinderen',
          value: dataSet.HuishoudensZonderKinderen || 0,
        },
        {
          key: 'Met Kinderen',
          value: dataSet.HuishoudensMetKinderen || 0,
        },
      ]
    }

    if ('k_40HuishoudensMetLaagsteInkomen' in dataSet) {
      const gemiddeld = Math.round(
        100 -
          dataSet.k_40HuishoudensMetLaagsteInkomen -
          dataSet.k_20HuishoudensMetHoogsteInkomen
      )
      inkomensNiveau = [
        {
          key: '40% laagste',
          value: dataSet.k_40HuishoudensMetLaagsteInkomen || 0,
        },

        {
          key: 'Gemiddeld',
          value: gemiddeld || 0,
        },
        {
          key: '20% hoogste',
          value: dataSet.k_20HuishoudensMetHoogsteInkomen || 0,
        },
      ]
    }

    return [inwoners, huishoudens, inkomensNiveau]
  }, [dataSet, width])

  return (
    <div className="PanelContent" key="sociaalContent">
      <DataView>
        <DataItem
          value1={dataSet.GemiddeldInkomenPerInwoner}
          value2=""
          label="Gemiddeld inkomen per inwoner"
          unit="x 1000 &euro;"
        />

        {InkomensNiveauData.length > 0 && (
          <>
            <h4 className="graphTitle">
              Verdeling inkomensniveau per huishouden:
            </h4>
            <Grafiek
              key="graph3"
              dataSet={InkomensNiveauData}
              width={width}
              barHeight={25}
              time={750}
              colors={['#0B71A1', '#7794A1']}
            />
            <p className="small">
              Percentage Huishoudens behorend tot laagste 40%, gemiddeld, 20%
              hoogste inkomens groep
            </p>
          </>
        )}
      </DataView>

      <DataView>
        <DataItem
          value1={dataSet.AantalInwoners}
          value2=""
          label="Aantal Inwoners"
          unit=""
        />
        {InwonersData.length > 0 && (
          <>
            <h4 className="graphTitle">Verdeling Inwoners naar leeftijd</h4>
            <Graph
              key="graph1"
              dataSet={InwonersData}
              colors={['#0B71A1']}
              margin={{ top: 0, right: 40, bottom: 0, left: 95 }}
              height={130}
              width={width}
              time={750}
            />
          </>
        )}
      </DataView>
      <DataView>
        <DataItem
          value1={dataSet.HuishoudensTotaal}
          value2=""
          label="Totaal particuliere huishoudens"
          unit=""
        />
        <DataItem
          value1={dataSet.GemiddeldeHuishoudensgrootte}
          value2=""
          label="Gemiddelde huishoudensgrootte"
          unit=""
          infohover="Dit gemiddelde is berekend als het aantal in particuliere
              huishoudens levende personen gedeeld door het aantal
              particuliere huishoudens."
        />
        {HuishoudenData.length > 0 && (
          <>
            <h4 className="graphTitle">Verdeling type huishoudens</h4>
            <Graph
              key="graph2"
              dataSet={HuishoudenData}
              colors={['#0B71A1']}
              margin={{ top: 0, right: 40, bottom: 0, left: 95 }}
              height={80}
              width={width}
              time={750}
            />
          </>
        )}
      </DataView>
    </div>
  )
}

PanelS.propTypes = {
  dataSet: object,
  width: number,
}

export default PanelS
