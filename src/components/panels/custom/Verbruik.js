// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, object } from 'prop-types'
import React, { useEffect, useState } from 'react'

import DataItem from '../asset/DataItem'
import { DataView } from '../PanelStyle'
import Graph from './../asset/graph-vertical'

function PanelV({ dataSet, width }) {
  const [dataElek, setDataElek] = useState()
  const [dataGas, setDataGas] = useState()

  useEffect(() => {
    if (dataSet && Object.values(dataSet).length > 0) {
      setDataElek([
        {
          key: 'Appartement',
          value: dataSet.Gemiddeld_Elektriciteitsverbruik_Appartement || 0,
        },
        {
          key: 'Tussen woning',
          value: dataSet.Gemiddeld_Elektriciteitsverbruik_Tussenwoning || 0,
        },
        {
          key: 'Hoek woning',
          value: dataSet.Gemiddeld_Elektriciteitsverbruik_Hoekwoning || 0,
        },
        {
          key: 'TweeOnder EenKap ',
          value: dataSet.Gemiddeld_Elektriciteitsverbruik_2_onder_1_kap || 0,
        },
        {
          key: 'Vrijstaand',
          value: dataSet.Gemiddeld_Elektriciteitsverbruik_Vrijstaand || 0,
        },
      ])
      setDataGas([
        {
          key: 'Appartement',
          value: dataSet.Gemiddeld_Gasverbruik_Appartement || 0,
        },
        {
          key: 'Tussen woning',
          value: dataSet.Gemiddeld_Gasverbruik_Tussenwoning || 0,
        },
        {
          key: 'Hoek woning',
          value: dataSet.Gemiddeld_Gasverbruik_Hoekwoning || 0,
        },
        {
          key: 'TweeOnder EenKap ',
          value: dataSet.Gemiddeld_Gasverbruik_2_onder_1_kap || 0,
        },
        {
          key: 'Vrijstaand',
          value: dataSet.Gemiddeld_Gasverbruik_Vrijstaand || 0,
        },
      ])
    } else {
      setDataElek([])
      setDataGas([])
    }
  }, [dataSet])

  return (
    <div className="PanelContent" key="EnergieVerbruikContent">
      <DataItem
        value1={dataSet.Gemiddeld_Elektriciteitsverbruik}
        value2=""
        label="Gemiddeld Electriciteits verbruik"
        unit="kWh"
      />
      <DataItem
        value1={dataSet.Gemiddeld_Gasverbruik}
        value2=""
        label="Gemiddeld Aardgas verbruik"
        unit="m3"
      />
      <DataItem
        value1={dataSet.Percentage_Stadsverwarming}
        value2=""
        label="Woningen met Stadsverwarming"
        unit="%"
      />

      {dataElek && dataGas && dataElek.length > 0 && dataGas.length > 0 && (
        <DataView>
          <h4 className="graphTitle">Verbruik naar woning type</h4>
          <div>
            <Graph
              dataSet={dataElek}
              colors={['#ffbc2c']}
              height={200}
              width={Math.floor(width / 2) - 10}
              margin={{ top: 10, right: 0, bottom: 90, left: 35 }}
              time={750}
              average={dataSet.GemiddeldeElektriciteitslevering}
            />
            <h5 className="graphNote">
              Electriciteit (
              <span style={{ color: '#ffbc2c', fontWeight: 'bold' }}>kwh</span>)
            </h5>
          </div>
          <div>
            <Graph
              dataSet={dataGas}
              colors={['#0B71A1']}
              height={200}
              width={Math.floor(width / 2)}
              margin={{ top: 10, right: 0, bottom: 90, left: 35 }}
              time={750}
              average={dataSet.Gemiddeld_Gasverbruik}
            />
            <h5 className="graphNote">
              Gas (
              <span style={{ color: '#0B71A1', fontWeight: 'bold' }}>m3</span>)
            </h5>
          </div>
        </DataView>
      )}
    </div>
  )
}

PanelV.propTypes = {
  dataSet: object,
  width: number,
}

export default PanelV
