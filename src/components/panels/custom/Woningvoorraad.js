// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, object } from 'prop-types'
import React, { useMemo } from 'react'

import BarGrafiek from '../asset/bar-div-100-perc'
import DataItem from '../asset/DataItem'
import { DataView } from '../PanelStyle'
import GrafiekWoning2 from './../asset/graph-diverging'

function PanelW({ dataSet, width }) {
  // Extract the relevant data for this panel from the entire set of data.
  const [dataVoorraad, dataPeriode, dataTypeWoning] = useMemo(() => {
    let dataVoorraad, dataPeriode, dataTypeWoning

    if (!dataSet || Object.values(dataSet).length == 0) {
      return [0, [], [], []]
    }

    dataVoorraad = [
      {
        key: 'Corporatiebezit',
        value: dataSet.wv_percentage_TypeEigenaar_CorporatieBezit || 0,
      },
      {
        key: 'Particulier bezit',
        value: dataSet.wv_percentage_TypeEigenaar_ParticulierBezit || 0,
      },
      {
        key: 'Overig',
        value:
          dataSet.wv_percentage_TypeEigenaar_OverigHuurBezit +
            dataSet.wv_percentage_TypeEigenaar_OverigBezit || 0,
      },
      // FIX: Gives rendering trouble since the percentage is so small.
      // {
      //   key: 'Onbekend',
      //   value: dataSet.wv_percentage_TypeEigenaar_OverigBezit || 0,
      // },
    ]

    if (
      dataSet.wv_percentage_TypeEigenaar_CorporatieBezit ||
      dataSet.wv_percentage_TypeEigenaar_ParticulierBezit ||
      dataSet.wv_percentage_TypeEigenaar_OverigHuurBezit
    ) {
      dataPeriode = [
        {
          key: 'Voor 1946',
          value: dataSet.wv_aantal_Cooperatie_Bouwperiode_Voor_1946 || 0,
          value2: dataSet.wv_aantal_Particulier_Bouwperiode_Voor_1946 || 0,
        },
        {
          key: '1946 t/m 1974',
          value: dataSet.wv_aantal_Cooperatie_Bouwperiode_1946_1974 || 0,
          value2: dataSet.wv_aantal_Particulier_Bouwperiode_1946_1974 || 0,
        },
        {
          key: '1975 t/m 1991',
          value: dataSet.wv_aantal_Cooperatie_Bouwperiode_1975_1991 || 0,
          value2: dataSet.wv_aantal_Particulier_Bouwperiode_1975_1991 || 0,
        },
        {
          key: '1992 t/m 2006',
          value: dataSet.wv_aantal_Cooperatie_Bouwperiode_1992_2006 || 0,
          value2: dataSet.wv_aantal_Particulier_Bouwperiode_1992_2006 || 0,
        },
        {
          key: 'na 2006',
          value: dataSet.wv_aantal_Cooperatie_Bouwperiode_2006_later || 0,
          value2: dataSet.wv_aantal_Particulier_Bouwperiode_2006_later || 0,
        },
      ]

      dataTypeWoning = [
        {
          key: 'Appartement',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_Appartement || 0,
          value2: dataSet.wv_aantal_Particulier_TypeWoning_Appartement || 0,
        },
        {
          key: 'Tussenwoning',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_Tussenwoning || 0,
          value2: dataSet.wv_aantal_Particulier_TypeWoning_Tussenwoning || 0,
        },
        {
          key: 'Hoekwoning',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_Hoekwoning || 0,
          value2: dataSet.wv_aantal_Particulier_TypeWoning_Hoekwoning || 0,
        },
        {
          key: 'Twee-onder-één-kap',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_TweeOnderEenKap || 0,
          value2: dataSet.wv_aantal_Particulier_TypeWoning_TweeOnderEenKap || 0,
        },
        {
          key: 'Vrijstaande woning',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_VrijstaandeWoning || 0,
          value2:
            dataSet.wv_aantal_Particulier_TypeWoning_VrijstaandeWoning || 0,
        },
        {
          key: 'Overig',
          value: dataSet.wv_aantal_Cooperatie_TypeWoning_Overig || 0,
          value2: dataSet.wv_aantal_Particulier_TypeWoning_Overig || 0,
        },
      ]
    }

    return [dataVoorraad, dataPeriode, dataTypeWoning]
  }, [dataSet])

  return (
    <div className="PanelContent" key="woningvoorraadContent">
      <DataItem
        value1=""
        value2={dataSet.wv_totaal_WoningVoorraad}
        label="Totale Woningvoorraad"
        unit=""
        infohover="Het totale aantal woningen op 1 januari van het desbetreffende
              jaar. Een woning is een verblijfsobject met minimaal een
              woonfunctie en eventueel één of meer andere gebruiksfuncties."
      />
      <DataItem
        value1={dataSet.GemiddeldeWOZWaardeVanWoningen}
        value2=""
        label="Gemiddelde Woningwaarde"
        unit="x 1000 &euro;"
        infohover="Het totale aantal woningen op 1 januari van het desbetreffende
              jaar. Een woning is een verblijfsobject met minimaal een
              woonfunctie en eventueel één of meer andere gebruiksfuncties."
      />

      <DataView>
        {dataVoorraad && dataVoorraad.length > 0 && (
          <>
            <h4 className="graphTitle">Verdeling Woningvoorraad</h4>
            <BarGrafiek
              dataSet={dataVoorraad}
              width={width}
              colors={['#0B71A1', '#7794A1']}
              barHeight={25}
              time={750}
            />
          </>
        )}
      </DataView>
      <div className="dataView">
        <h4 className="graphTitle">Verdeling Bouwperiode</h4>
        {dataPeriode && dataPeriode.length > 0 && (
          <GrafiekWoning2
            key="graphBouwperiode"
            dataSet={dataPeriode}
            width={width}
            height={dataPeriode.length * 20}
            margin={{ top: 20, right: 0, bottom: 0, left: 105 }}
            colors={['#0B71A1', '#7794A1']}
            time={750}
            graphTitle="Verdeling Woningvoorraad"
            labels={['Corporatiebezit', 'Particulier bezit']}
          />
        )}
        {dataPeriode && dataPeriode.length === 0 && (
          <p style={{ fontWeight: 'bold', color: '#0b71a1' }}>Geen data</p>
        )}
      </div>

      <div className="dataView">
        <h4 className="graphTitle">Verdeling Woningtype</h4>
        {dataPeriode && dataPeriode.length > 0 && (
          <GrafiekWoning2
            key="graphtypeWoning"
            dataSet={dataTypeWoning}
            width={width}
            height={dataTypeWoning.length * 20}
            margin={{ top: 20, right: 0, bottom: 0, left: 105 }}
            colors={['#0B71A1', '#7794A1']}
            time={750}
            graphTitle="Verdeling Woningvoorraad"
            labels={['Corporatiebezit', 'Particulier bezit']}
          />
        )}
        {dataPeriode && dataPeriode.length === 0 && (
          <p style={{ fontWeight: 'bold', color: '#0b71a1' }}>Geen data</p>
        )}
      </div>
    </div>
  )
}

PanelW.propTypes = {
  dataSet: object,
  width: number,
}

export default PanelW
