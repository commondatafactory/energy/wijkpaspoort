// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, object, string } from 'prop-types'
import React, { useEffect, useState } from 'react'

import DataItem from '../asset/DataItem'
import * as Styled from '../PanelStyle'

function PanelU({ regioCode, dataSet, width }) {
  const [vizData, setVizData] = useState()

  const customIcon = {
    School: '/img/school.svg',
    Sport: '/img/sport.svg',
    Zorg: '/img/zorg.svg',
    Kantoren: '/img/kantoor.svg',
    Bijeenkomst: '/img/bijeenkomst.svg',
    Logies: '/img/logies.svg',
    Winkels: '/img/winkel.svg',
    Industrie: '/img/industrie.svg',
    Wonen: '/img/huis.svg',
  }

  useEffect(() => {
    if (dataSet) {
      const newData = [
        {
          key: 'School',
          value: dataSet.onderwijsfunctie || 0,
        },
        {
          key: 'Zorg',
          value: dataSet.gezondheidszorgfunctie || 0,
        },
        {
          key: 'Bijeenkomst',
          value: dataSet.bijeenkomstfunctie || 0,
        },
        {
          key: 'Logies',
          value: dataSet.logiesfunctie || 0,
        },
        {
          key: 'Winkels',
          value: dataSet.winkelfunctie || 0,
        },
        {
          key: 'Kantoren',
          value: dataSet.kantoorfunctie || 0,
        },
        {
          key: 'Sport',
          value: dataSet.sportfunctie || 0,
        },
        {
          key: 'Industrie',
          value: dataSet.industriefunctie || 0,
        },
      ]
      setVizData(newData)
    }
  }, [dataSet])

  return (
    <Styled.DataView>
      <Styled.UtiliteitDiv key={regioCode} style={{ width: width - 40 + 'px' }}>
        {vizData &&
          vizData.map((item) => (
            <div key={item.key} className="utiliteitItem">
              <img
                alt="icon"
                width="36"
                height="36"
                className="icon"
                src={customIcon[item.key]}
              />
              <DataItem
                value1={item.value}
                label={item.key}
                unit=""
                key={item.key}
              />
            </div>
          ))}
      </Styled.UtiliteitDiv>
    </Styled.DataView>
  )
}

PanelU.propTypes = {
  regioCode: string,
  dataSet: object,
  width: number,
}

export default PanelU
