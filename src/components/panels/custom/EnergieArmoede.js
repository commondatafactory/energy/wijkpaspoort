// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { number, object } from 'prop-types'
import React, { useContext, useEffect, useState } from 'react'

import { AppContext } from '../../geo-navigatie/GeoNavigatieContextAPI'
import { DataView, EnergieArmoedeTitle } from '../PanelStyle'
import Grafiek from './../asset/horizontalBarsDouble'

function PanelArmoede({ dataSet, width }) {
  const { center } = useContext(AppContext)

  const [MonitorEnergieArmoede, setMonitorEnergieArmoede] = useState()

  useEffect(() => {
    setMonitorEnergieArmoede([])
    if (dataSet && Object.values(dataSet).length > 0) {
      //Voor de juiste volgorde
      const namen = [
        'Totaal',
        'Koop',
        'Huur, corporatie',
        'Huur, overig',
        'Onbekend',
      ]
      const lihelekDataset = []
      for (let i = 0; i < namen.length; i++) {
        const o = {
          key: namen[i].replace(/[, ] /g, '').toLocaleLowerCase(),
          naam: namen[i],
          percentage:
            parseFloat(
              dataSet[namen[i]][0].energiearmoede_lihelek2.replace(/,/g, '.')
            ) || 0,
          aantal:
            parseFloat(
              dataSet[namen[i]][0].aantal_huishoudens1.replace(/,/g, '.')
            ) * 1000 || 0,
        }
        lihelekDataset.push(o)
      }
      setMonitorEnergieArmoede(lihelekDataset)
    } else {
      setMonitorEnergieArmoede([])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet, width])

  return (
    <div className="PanelContent" key="armoedeContent">
      <DataView>
        {MonitorEnergieArmoede?.length > 0 && (
          <>
            <p>
              Huishoudens met een laag inkomen en een hoge energierekening en/of
              met een woning van lage energetische kwaliteit. Een combinatie van
              de indicatoren LIHE en LILEK. Bovenaan het totaal percentage
              huishoudens op het absolute aantal huishoudens in de regio
              (huishoudens die meegeteld worden). Daaronder nogmaals opgesplitst
              naar type eigenaar.{' '}
            </p>
            <h4 className="graphTitle">Monitor Energiearmoede 2021 LIHELEK</h4>
            <EnergieArmoedeTitle>
              <p>% LIHELEK</p>
              <p>Aantal huishoudens</p>
            </EnergieArmoedeTitle>
            <Grafiek
              key="grapharmoede"
              dataSet={MonitorEnergieArmoede}
              width={width}
              height={200}
              time={750}
            />
            <p>
              {' '}
              Wij tonen hier alleen de indicator LIHELEK. Voor meer indicatoren
              zie{' '}
              <a
                title="Bekijk de kaart in de DEGO viewer"
                target="blank"
                href={`https://dego.vng.nl/?tab=armoede&label=topo&layer=layer305&sublayer=Alle+huishoudens#14/${center[1]}/${center[0]}`}
              >
                DEGO
                <span>
                  {' '}
                  <img
                    alt="External link"
                    title="Bekijk de kaart in de DEGO viewer, externe link"
                    className="icon"
                    src="/img/external-link-line.svg"
                    style={{ height: '18px', width: '18px' }}
                  />
                </span>
              </a>{' '}
              of het{' '}
              <a
                target="blank"
                href="https://www.cbs.nl/nl-nl/maatwerk/2023/47/monitor-energiearmoede-2021"
              >
                CBS
              </a>{' '}
              <span>
                {' '}
                <img
                  alt="External link"
                  title="Bekijk de kaart in de DEGO viewer, externe link"
                  className="icon"
                  src="/img/external-link-line.svg"
                  style={{ height: '18px', width: '18px' }}
                />
              </span>
              .
            </p>
          </>
        )}

        {MonitorEnergieArmoede?.length === 0 && (
          <p style={{ fontWeight: 'bold', color: '#0b71a1' }}>Geen data</p>
        )}
      </DataView>
    </div>
  )
}

PanelArmoede.propTypes = {
  dataSet: object,
  width: number,
}

export default PanelArmoede
