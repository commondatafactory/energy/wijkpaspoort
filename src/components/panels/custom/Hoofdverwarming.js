// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useEffect, useState } from 'react'

import { getTypeVerwarmingsinstallatie } from '../../../services/hvi'
import Blocks from './../asset/blocks-100-percent'

function PanelHVI({ dataSet, width }) {
  const [typeList, setTypeList] = useState()
  const [vizDataSet, setVizDataset] = useState()

  const colors = [
    '#1f77b4',
    '#ff7f0e',
    '#d62728',
    '#2ca02c',
    '#17becf',
    '#9467bd',
    '#e377c2',
    '#bcbd22',
    '#7f7f7f',
    '#8c564b',
  ]

  useEffect(() => {
    Promise.all([getTypeVerwarmingsinstallatie()]).then((data) =>
      setTypeList(data[0])
    )
  }, [])

  useEffect(() => {
    if (dataSet && dataSet.length >= 1 && typeList) {
      // Give each type a color
      for (let i = 0; i < Object.keys(typeList).length; i++) {
        typeList[i].color = colors[i]
      }
      // Filter op waarden boven 0
      let newDataset = dataSet.filter((element) => {
        return element.value >= 1
      })

      // Sorteer groot naar klein
      Object.values(newDataset).sort(function (a, b) {
        return b - a
      })

      // Remove class to make 100%
      newDataset = newDataset
        .filter((e) => e.key !== 'Totaal')
        .map((e) => {
          let temp = typeList.find((element) => element.Title === e.title)
          return {
            ...e,
            color: temp.color,
          }
        })
      setVizDataset(newDataset)
    } else {
      setVizDataset()
    }
  }, [dataSet, typeList])

  return (
    <div className="PanelContent" key="hviContent">
      <p>
        Dit paneel geeft inzicht in de hoofdverwarmingsinstallaties van
        woningen, en geeft weer welk deel van de woningen een individuele CV
        heeft, op een warmtenet is aangesloten, een warmtepomp heeft of op een
        andere manier verwarmt.{' '}
      </p>
      {/* {vizDataSet && vizDataSet.length > 0 && (
        <Grafiek key="graphhvi" dataSet={vizDataSet} width={width} barHeight={25} time={750} colors={colors} />
      )} */}

      {vizDataSet && vizDataSet.length > 0 && (
        <Blocks
          key="graphblockhvi"
          dataSet={vizDataSet}
          width={width * 0.5}
          height={width * 0.5}
          margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
          time={750}
          title="Aandeel per type hoofdverwarmingsinstallatie [totaal: 100%]"
        />
      )}
    </div>
  )
}

// PanelHVI.propTypes = {
//   width: number,
// }

export default PanelHVI
