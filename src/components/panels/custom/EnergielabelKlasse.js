// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { number, object, string } from 'prop-types'
import React, { useEffect, useState } from 'react'

import DataItem from '../asset/DataItem'
import Graph from '../asset/graph-vertical'
import { DataView } from '../PanelStyle'

function PanelEL({ dataSet, regioCode, regio, width }) {
  const [vizDataSet, setDataSet] = useState()
  const [totalLabels, settotalLabels] = useState(0)

  const emptyData = [
    { key: 'A++++ ( < 0 )', value: 0 },
    { key: 'A+++ ( 1 - 50 )', value: 0 },
    { key: 'A++ ( 51 - 80 )', value: 0 },
    { key: 'A+ ( 81 - 110 )', value: 0 },
    { key: 'A ( 111 - 165 )', value: 0 },
    { key: 'B ( 166 - 195 )', value: 0 },
    { key: 'C ( 196 - 255 )', value: 0 },
    { key: 'D ( 256 - 300 )', value: 0 },
    { key: 'E ( 301 - 345 )', value: 0 },
    { key: 'F ( 346 - 390 )', value: 0 },
    { key: 'G ( 391 < )', value: 0 },
  ]

  useEffect(() => {
    if (regioCode) {
      setDataSet(emptyData)
      if (dataSet && Object.values(dataSet).length > 0) {
        setDataSet([
          { key: 'A++++ ( < 0 )', value: dataSet['A++++'] || 0 },
          { key: 'A+++ ( 1 - 50 )', value: dataSet['A+++'] || 0 },
          { key: 'A++ ( 51 - 80 )', value: dataSet['A++'] || 0 },
          { key: 'A+ ( 81 - 110 )', value: dataSet['A+'] || 0 },
          { key: 'A ( 111 - 165 )', value: dataSet.A || 0 },
          { key: 'B ( 166 - 195 )', value: dataSet.B || 0 },
          { key: 'C ( 196 - 255 )', value: dataSet.C || 0 },
          { key: 'D ( 256 - 300 )', value: dataSet.D || 0 },
          { key: 'E ( 301 - 345 )', value: dataSet.E || 0 },
          { key: 'F ( 346 - 390 )', value: dataSet.F || 0 },
          { key: 'G ( 391 < )', value: dataSet.G || 0 },
        ])
        settotalLabels(
          (dataSet['A++++'] || 0) +
            (dataSet['A+++'] || 0) +
            (dataSet['A++'] || 0) +
            (dataSet['A+'] || 0) +
            (dataSet.A || 0) +
            (dataSet.B || 0) +
            (dataSet.C || 0) +
            (dataSet.D || 0) +
            (dataSet.E || 0) +
            (dataSet.F || 0) +
            (dataSet.G || 0)
        )
      } else {
        setDataSet(emptyData) // response is empty
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSet, width])

  return (
    <div key="energielabelContent" className="PanelContent">
      {vizDataSet && (
        <>
          <DataItem
            value1={totalLabels}
            value2=""
            label={`Totaal Aantal Energielabelklasse in ${regio}`}
            unit=""
          />
          <DataView>
            <h4 className="graphTitle">Verdeling Energie label Klasse </h4>
            <Graph
              dataSet={vizDataSet}
              colors={[
                '#009342',
                '#009342',
                '#009342',
                '#009342',
                '#009342',
                '#1ba943',
                '#9ecf1b',
                '#f8f51c',
                '#f4b003',
                '#df6d14',
                '#db261d',
              ]}
              margin={{ top: 5, right: 5, bottom: 80, left: 35 }}
              width={width}
              height={400}
              time={750}
            />
          </DataView>
        </>
      )}
    </div>
  )
}

PanelEL.propTypes = {
  regioCode: string,
  regio: string,
  width: number,
  dataSetWoningType: object,
  dataSet: object,
}

export default PanelEL
