// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useEffect, useState } from 'react'

import HorzGraph from '../asset/bar-div-100-perc'
import DataItem from '../asset/DataItem'
import StackedGraph from '../asset/graph-stacked'
import * as Styled from '../PanelStyle'

function PanelLevensloop({ woningTypeDataSet, bouwPeriodeDataSet, width }) {
  const [vizWoningTypeDataset, setVizWoningTypeDataset] = useState([])
  const [vizBouwPeriodeDataset, setVizBouwPeriodeDataset] = useState([])
  const [totaalDataset, setTotaalDataset] = useState()
  const [totaal, setTotaal] = useState(0)
  const colors = ['#8c564b', '#7794a1', '#17becf', '#1f77b4']

  // type woning
  const groups = [
    'Eigen woning',
    'Woningcorporatie',
    'Overige verhuur',
    'Geen woning',
  ]
  // bouwperiode
  const groups2 = [
    'Tot 1945',
    '1945-1975',
    '1976-1995',
    '1996-2005',
    'vanaf 2006',
    'Onbekend',
  ]
  // leeftijdsgroepen
  const subGroups = [
    'overig',
    'hoofdbewoner_1955_1970',
    'hoofdbewoner_1945_1955',
    'hoofdbewoner_0_1945',
  ]

  const currentYear = new Date().getFullYear()

  const subGroupsNice = [
    `0 t/m ${currentYear - 1970}`,
    ` ${currentYear - 1970} t/m ${currentYear - 1955}`,
    ` ${currentYear - 1955} t/m ${currentYear - 1945}`,
    ` ${currentYear - 1945} en ouder`,
  ]
  useEffect(() => {
    if (woningTypeDataSet && Object.values(woningTypeDataSet).length) {
      let totaalSet = woningTypeDataSet.Totaal.filter((obj) => {
        return obj.Bouwperiode_woning === 'Totaal'
      })
      totaalSet = totaalSet[0]
      Object.keys(totaalSet).forEach((key) => {
        totaalSet[key] =
          typeof totaalSet[key] === 'string' || totaalSet[key] >= 0
            ? totaalSet[key]
            : 0
      })

      totaalSet.overig =
        totaalSet.Totaal_huishoudens -
        totaalSet.hoofdbewoner_0_1945 -
        totaalSet.hoofdbewoner_1945_1955 -
        totaalSet.hoofdbewoner_1955_1970

      let totaalSet2 = [
        {
          key: `0 t/m ${currentYear - 1970}`,
          value: totaalSet.overig,
        },
        {
          key: ` ${currentYear - 1970} t/m ${currentYear - 1955}`,
          value: totaalSet.hoofdbewoner_1955_1970,
        },
        {
          key: ` ${currentYear - 1955} t/m ${currentYear - 1945}`,
          value: totaalSet.hoofdbewoner_1945_1955,
        },
        {
          key: `${currentYear - 1945} en ouder`,
          value: totaalSet.hoofdbewoner_0_1945,
        },
      ]

      totaalSet2.forEach((elm) => {
        elm.value = Math.round((elm.value * 100) / totaalSet.Totaal_huishoudens)
      })

      setTotaal(totaalSet2[1].value + totaalSet2[2].value + totaalSet2[3].value)
      setTotaalDataset(totaalSet2)
    } else {
      setTotaal(null)
      setTotaalDataset(null)
    }
  }, [woningTypeDataSet, width])

  // Dataset voor overzicht aantal ouderen per type woonsituatie.
  useEffect(() => {
    if (woningTypeDataSet && Object.values(woningTypeDataSet).length) {
      const newDataAgrregated = []
      Object.keys(woningTypeDataSet).forEach((key) => {
        let element = woningTypeDataSet[key]
        let totaal = element.filter((obj) => {
          return obj.Bouwperiode_woning === 'Totaal'
        })
        if (totaal[0].Type_eigendom_woning !== 'Totaal') {
          newDataAgrregated.push({
            key: key,
            hoofdbewoner_0_1945:
              totaal[0].hoofdbewoner_0_1945 >= 0
                ? totaal[0].hoofdbewoner_0_1945
                : 0,
            hoofdbewoner_1945_1955:
              totaal[0].hoofdbewoner_1945_1955 >= 0
                ? totaal[0].hoofdbewoner_1945_1955
                : 0,
            hoofdbewoner_1955_1970:
              totaal[0].hoofdbewoner_1955_1970 >= 0
                ? totaal[0].hoofdbewoner_1955_1970
                : 0,
            overig:
              (totaal[0].Totaal_huishoudens >= 0
                ? totaal[0].Totaal_huishoudens
                : 0) -
              (totaal[0].hoofdbewoner_0_1945 >= 0
                ? totaal[0].hoofdbewoner_0_1945
                : 0) -
              (totaal[0].hoofdbewoner_1945_1955 >= 0
                ? totaal[0].hoofdbewoner_1945_1955
                : 0) -
              (totaal[0].hoofdbewoner_1955_1970 >= 0
                ? totaal[0].hoofdbewoner_1955_1970
                : 0),
          })
        }
      })
      setVizWoningTypeDataset(newDataAgrregated)
    } else {
      setVizWoningTypeDataset()
    }
  }, [woningTypeDataSet, width])

  useEffect(() => {
    if (bouwPeriodeDataSet && Object.values(bouwPeriodeDataSet).length > 0) {
      const newDataAgrregated = []
      Object.keys(bouwPeriodeDataSet).forEach((key) => {
        let element = bouwPeriodeDataSet[key]
        let totaal = element.filter((obj) => {
          return obj.Type_eigendom_woning === 'Totaal'
        })

        if (totaal[0].Bouwperiode_woning !== 'Totaal') {
          newDataAgrregated.push({
            key: key,
            hoofdbewoner_0_1945:
              totaal[0].hoofdbewoner_0_1945 >= 0
                ? totaal[0].hoofdbewoner_0_1945
                : 0,
            hoofdbewoner_1945_1955:
              totaal[0].hoofdbewoner_1945_1955 >= 0
                ? totaal[0].hoofdbewoner_1945_1955
                : 0,
            hoofdbewoner_1955_1970:
              totaal[0].hoofdbewoner_1955_1970 >= 0
                ? totaal[0].hoofdbewoner_1955_1970
                : 0,
            overig:
              (totaal[0].Totaal_huishoudens >= 0
                ? totaal[0].Totaal_huishoudens
                : 0) -
              (totaal[0].hoofdbewoner_0_1945 >= 0
                ? totaal[0].hoofdbewoner_0_1945
                : 0) -
              (totaal[0].hoofdbewoner_1945_1955 >= 0
                ? totaal[0].hoofdbewoner_1945_1955
                : 0) -
              (totaal[0].hoofdbewoner_1955_1970 >= 0
                ? totaal[0].hoofdbewoner_1955_1970
                : 0),
          })
        }
      })
      setVizBouwPeriodeDataset(newDataAgrregated)
    } else {
      setVizBouwPeriodeDataset()
    }
  }, [bouwPeriodeDataSet, width])

  return (
    <div className="PanelContent" key="hviContent">
      <Styled.DataView>
        <DataItem
          value1={totaal}
          value2=""
          label="huishoudens van 53 en ouder"
          unit="%"
        />
        {totaalDataset && totaalDataset.length && (
          <>
            <h4 className="graphTitle">Verdeling Leeftijdsgroepen:</h4>
            <HorzGraph
              key="graph3"
              dataSet={totaalDataset}
              width={width}
              barHeight={25}
              time={750}
              colors={colors}
            />
          </>
        )}
      </Styled.DataView>
      <Styled.DataView>
        {vizBouwPeriodeDataset &&
          Object.values(vizBouwPeriodeDataset).length && (
            <>
              <h4 className="graphTitle">
                Verdeling leeftijdsgroepen over bouwperiode woning
              </h4>
              <StackedGraph
                key="graphlevensloop2"
                dataSet={vizBouwPeriodeDataset}
                width={width}
                height={400}
                time={750}
                margin={{ top: 10, right: 10, bottom: 70, left: 60 }}
                colors={colors}
                groups={groups2}
                subgroups={subGroups}
              />
            </>
          )}

        {vizWoningTypeDataset && Object.values(vizWoningTypeDataset).length && (
          <>
            <h4 className="graphTitle">
              Verdeling leeftijdsgroepen over woningtype woning
            </h4>
            <StackedGraph
              key="graphlevensloop1"
              dataSet={vizWoningTypeDataset}
              width={width}
              height={400}
              time={750}
              margin={{ top: 10, right: 10, bottom: 105, left: 60 }}
              colors={colors}
              groups={groups}
              subgroups={subGroups}
            />
          </>
        )}
        {vizWoningTypeDataset &&
          vizBouwPeriodeDataset &&
          Object.values(vizWoningTypeDataset).length &&
          Object.values(vizBouwPeriodeDataset).length && (
            <>
              <h4 className="graphTitle">Leeftijdsgroepen</h4>
              <div style={{ display: 'flex' }}>
                {subGroupsNice.map((element, i) => (
                  <div key={element + 'legend' + i} className="legendItem">
                    <div
                      key={element + 'block' + i}
                      className="legendBlock"
                      style={{
                        backgroundColor: colors[i],
                        width: 25 + 'px',
                        height: 15 + 'px',
                      }}
                    />
                    <p key={element + 'color' + i}>{element}</p>
                  </div>
                ))}
              </div>
            </>
          )}
      </Styled.DataView>
    </div>
  )
}

export default PanelLevensloop
