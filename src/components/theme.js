// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { defaultTheme } from '@commonground/design-system'

const tokens = {
  ...defaultTheme.tokens,
}

const theme = {
  ...defaultTheme,

  tokens,

  // Generics
  colorBackground: '#00FF00',
  niene: '#00ffee',
}

export default theme
