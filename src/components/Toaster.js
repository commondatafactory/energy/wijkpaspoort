// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { Alert } from '@commonground/design-system'
import React, { useEffect, useRef, useState } from 'react'
import styled, { keyframes } from 'styled-components'

const onMount = keyframes`
  0% { transform: translateX(-100px); opacity:0;}
  100% { transform: none; opacity:1;}
`

const onUnmount = keyframes`
  0% { transform: none; opacity:1;}
  100% { transform: translateX(-100px); opacity:0;}
`

export const StyledToaster = styled(Alert)`
  animation: ${({ unmount }) => (unmount ? onUnmount : onMount)};
  animation-duration: ${({ unmount }) => (unmount ? '0.6s' : '0.3s')};

  box-shadow: rgb(0 0 0 / 25%) 3px 4px 6px 0px;
  display: flex;
  margin-bottom: 20px;
  padding: 1rem 1rem 1rem 3rem;
  width: 400px;
  z-index: 1000;

  p:first-child {
    font-weight: 700;
    margin: 0;
    font-size: 1rem;
  }

  > svg {
    :first-child {
      position: absolute;
      left: 1rem;
      color: ${({ variant, theme: { tokens } }) =>
        variant === 'success' ? tokens.colorSuccess : tokens.colorError};
    }
    :last-child {
      cursor: pointer;
      margin-left: auto;
    }
  }
`

export const Toaster = ({ title, body, variant, visibleTime = 5000, nth }) => {
  const [startUnmount, setStartUnmount] = useState(false)
  const [baseHeight, setBaseHeight] = useState(0)
  const ref = useRef()
  const [visible, setVisible] = useState(true)

  const handleRemoveToaster = () => {
    setVisible(false)
  }

  useEffect(() => {
    setTimeout(() => setStartUnmount(true), visibleTime - 300)
    setTimeout(() => handleRemoveToaster(), visibleTime)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (ref.current) {
      setBaseHeight(ref.current?.clientHeight)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref.current])

  return (
    <>
      {visible && (
        <StyledToaster
          baseHeight={baseHeight}
          nth={nth}
          ref={ref}
          unmount={startUnmount}
          variant={variant}
        >
          <div>
            {title && <p>{title}</p>}
            {body && <p>{body}</p>}
          </div>
        </StyledToaster>
      )}
    </>
  )
}
