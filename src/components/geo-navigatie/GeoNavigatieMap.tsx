// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'
import { Feature, FeatureCollection } from 'geojson'
import React, { FC, useContext, useEffect, useRef, useState } from 'react'

import { AppContext, levelElement } from './GeoNavigatieContextAPI'
import * as Styled from './GeoNavigatiePanel.styled'

interface MapProps {
  state: levelElement['state']
  active: levelElement['active']
  dataset: levelElement['data']
  highlight: levelElement['highlightCode']
  filterCode: levelElement['filterCode']
  itemWidth: number
  itemHeight: number
  name: levelElement['filterNaam']
}

export const Map: FC<MapProps> = ({
  state,
  active,
  dataset,
  highlight,
  filterCode,
  itemWidth,
  itemHeight,
  name,
}) => {
  const mapContainer = useRef(null)
  const { setHoverName, levelsAll, setCenter, setNextName, setActiveState } =
    useContext(AppContext)

  const [geoData, setGeoData] = useState<FeatureCollection>()

  // SVG settings
  useEffect(() => {
    d3.select(mapContainer.current)
      .attr('preserveAspectRatio', 'xMidYMin meet')
      .attr('viewBox', `0 0 ${itemWidth} ${itemHeight}`)
      .attr('title', name)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // get the data
  useEffect(() => {
    if (filterCode) {
      if (state === 0 && typeof dataset !== 'string') {
        setGeoData(dataset)
      } else if (state > 0 && state <= 1 && typeof dataset !== 'string') {
        const newDataset: Feature[] = dataset.features.filter(function (
          feature: Feature
        ) {
          return (
            feature.properties.provinciecode === levelsAll[state].filterCode
          )
        })

        setGeoData({
          type: 'FeatureCollection',
          features: newDataset,
        })
      } else if (state >= 2) {
        let dataSourceString = ''
        const filter = `&filter=<Filter><PropertyIsEqualTo><PropertyName>cbs:${levelsAll[state].filterTerm}</PropertyName><Literal>${filterCode}</Literal></PropertyIsEqualTo></Filter>`

        dataSourceString = dataset + filter
        if (dataSourceString.length >= 2) {
          ;(async () => {
            try {
              const data = await (await fetch(dataSourceString)).json()
              setGeoData(data)
            } catch (e) {
              console.error('error', e)
            }
          })()

          return
        }
      }
    }
  }, [name, filterCode, dataset])

  // Change of size
  useEffect(() => {
    if (mapContainer.current && geoData) {
      d3.select(mapContainer.current).attr(
        'viewBox',
        `0 0 ${itemWidth} ${itemHeight}`
      )
    }
  }, [itemHeight, itemWidth, geoData])

  // Move div
  useEffect(() => {
    d3.select(mapContainer.current)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [active])

  function click(feature) {
    const svg = d3.select(mapContainer.current)
    const t = d3.transition().duration(750).ease(d3.easeCubic)

    // Set coordinates for linking to TVW
    setCenter(d3.geoCentroid(feature))
    // Reset color
    svg.selectAll('path').style('fill', '#FFBC2C')
    // Highlight click
    d3.select(this).transition(t).style('fill', '#0b71a1')
    setNextName(
      // eslint-disable-next-line
      feature.properties[levelsAll[state].termnaam],
      // eslint-disable-next-line
      feature.properties[levelsAll[state].highlightTerm],
      state
    )

    setActiveState(state + 1)
  }
  function mouseover(e) {
    setHoverName(e.properties[levelsAll[state].termnaam])
  }
  function mouseout() {
    setHoverName('')
  }

  // Draw the map
  useEffect(() => {
    if (mapContainer.current && geoData) {
      const svg = d3.select(mapContainer.current)
      svg.selectAll('g').remove()
      const t = d3.transition().duration(750).ease(d3.easeCubic)

      // Projection & Path
      const projection = d3.geoMercator()
      projection.fitSize([itemWidth, itemHeight], geoData)
      const path = d3.geoPath().projection(projection)

      const g = svg.append('g').classed('map-layer', true)
      g.selectAll('path')
        .data(geoData.features)
        .enter()
        .append('path')
        .attr('d', path)
        .attr('class', 'geodata')
        .attr('id', (d) => {
          return 'id' + d.properties[levelsAll[state].highlightTerm]
        })
        .attr('title', (d) => {
          return d.properties[levelsAll[state].termnaam]
        })
      g.selectAll('path')
        .on('click', click)
        .on('mouseover', mouseover)
        .on('mouseout', mouseout)

      g.select('#id' + highlight)
        .transition(t)
        .delay(250)
        .style('fill', '#0b71a1')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [geoData, itemHeight, itemWidth])

  // Blue highlight path
  useEffect(() => {
    if (mapContainer.current) {
      const t = d3.transition().duration(750).ease(d3.easeCubic)

      const svg = d3.select(mapContainer.current)
      // Reset color
      if (active && state !== 3) {
        svg.selectAll('path').transition(t).style('fill', '#FFBC2C')
      } else {
        svg.selectAll('path').transition(t).style('fill', '#FFBC2C')
        svg
          .select('#id' + highlight)
          .transition(t)
          .delay(250)
          .style('fill', '#0b71a1')
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [active, highlight])

  return (
    <Styled.MapSVG
      ref={(el) => (mapContainer.current = el)}
      className={active ? 'large' : 'small'}
      key={filterCode}
    />
  )
}
