// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const MainNavPanel = styled.div`
  grid-row: 2 / span 1;
  grid-column: 1 / span 1;
  align-self: stretch;
  background-color: #ffffff;
  display: grid;
  grid-template-rows: 10vh 40vh 13vh;
  grid-template-columns: ${(div) => div.theme.tokens.spacing07} repeat(3, 1fr) ${(
      div
    ) => div.theme.tokens.spacing07};
  grid-template-areas:
    '. map0 map1 map2  .'
    '. bigMap bigMap bigMap  .'
    '. status status status  .';
`
export const HoverName = styled.h3`
  grid-area: bigMap;
  justify-self: center;
  align-self: end;
  bottom: 0;
  text-align: center;
  z-index: 5;
`

export const NavigationBar = styled.div`
  grid-row: 1 / span 1;
  grid-column: 1 / span 5;
  justify-self: stretch;
  background-color: ${(div) => div.theme.tokens.colorPaletteGray200};
`

export const MapDiv = styled.div`
  justify-self: stretch;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  &.large {
    margin-top: ${(div) => div.theme.tokens.spacing05};
  }
  &.small {
    padding: ${(div) => div.theme.tokens.spacing02};
  }
  transition: 0.2s ease;
  p {
    bottom: 0;
    text-align: center;
    color: ${(p) => p.theme.tokens.colorPaletteGray600};
    font-size: 0.8rem;
    margin: 0;
  }
`

export const ClickDiv = styled.div`
  justify-self: stretch;
  cursor: pointer;
  &:hover {
    opacity: 0.2;
    background-color: #0b71a1;
  }
  &:focus {
    outline: 2px var(--colorInfo) solid;
  }
`

export const FakeMapArea = styled.div`
  grid-area: bigMap;
  z-index: -100;
  margin-top: ${(div) => div.theme.tokens.spacing03};
`

export const MapSVG = styled.svg`
  width: 100%;
  &.large {
    z-index: 2;
  }

  &.small {
    pointer-events: none;
  }

  .geodata {
    stroke-width: 1;
    fill: #ffbc2c;
    stroke: #ebf0f7;
    cursor: pointer;
  }

  .geodata:hover {
    fill: #09618a !important;
  }
`
