// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useContext, useEffect, useRef, useState } from 'react'

import { AppContext, levelElement } from './GeoNavigatieContextAPI'
import { Map } from './GeoNavigatieMap'
import * as Styled from './GeoNavigatiePanel.styled'

function GeoNavigatieMaps() {
  const { activeAdminState, setEmptyName, levelsAll, setActiveState } =
    useContext(AppContext)

  const handleClick = (i: number) => {
    setActiveState(i)
    setEmptyName(i)
  }

  const fakeMapContainter = useRef(null)
  const [itemWidth, setItemWidth] = useState(-1)
  const [itemHeight, setItemHeight] = useState(-1)

  // holds the timer for setTimeout and clearInterval
  let MOVEMENT_TIMER = null

  // the number of ms the window size must stay the same size before the
  // dimension state variable is reset
  const RESET_TIMEOUT = 100

  const TEST_DIMENSIONS = () => {
    // For some reason targetRef.current.getBoundingClientRect was not available
    // I found this worked for me, but unfortunately I can't find the
    // documentation to explain this experience
    if (fakeMapContainter.current) {
      setItemWidth(fakeMapContainter.current.offsetWidth)
      setItemHeight(fakeMapContainter.current.offsetHeight)
    }
  }

  // This sets the dimensions on the first render
  useEffect(() => {
    TEST_DIMENSIONS()

    // every time the window is resized, the timer is cleared and set again
    // the net effect is the component will only reset after the window size
    // is at rest for the duration set in RESET_TIMEOUT.  This prevents rapid
    // redrawing of the component for more complex components such as charts
    window.addEventListener('resize', () => {
      clearInterval(MOVEMENT_TIMER)
      MOVEMENT_TIMER = setTimeout(TEST_DIMENSIONS, RESET_TIMEOUT)
    })
  }, [])

  return (
    <>
      <Styled.FakeMapArea ref={fakeMapContainter} key="fakemap" />
      {levelsAll.map((elem: levelElement, i: number) =>
        i <= activeAdminState ? (
          <Styled.MapDiv
            id={i}
            key={i}
            style={
              elem.active ? { gridArea: 'bigMap' } : { gridArea: `map${i}` }
            }
            className={elem.active ? 'large' : 'small'}
          >
            <Map
              active={elem.active}
              key={elem.admin}
              state={elem.state}
              dataset={elem.data}
              highlight={elem.highlightCode}
              filterCode={elem.filterCode}
              name={elem.filterNaam}
              itemWidth={itemWidth}
              itemHeight={itemHeight}
            />
            {!elem.active && <p>{elem.filterNaam}</p>}
          </Styled.MapDiv>
        ) : (
          ''
        )
      )}
      {levelsAll.map(
        (elem: levelElement, i: levelElement['state']) =>
          i <= activeAdminState &&
          !elem.active && (
            <Styled.ClickDiv
              style={{ gridArea: `map${i}` }}
              onClick={() => {
                handleClick(i)
              }}
              key={'clickdiv' + i}
            >
              {' '}
            </Styled.ClickDiv>
          )
      )}
    </>
  )
}

export default GeoNavigatieMaps
