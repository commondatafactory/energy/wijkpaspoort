// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FeatureCollection } from 'geojson'
import React, { createContext, Dispatch, SetStateAction, useState } from 'react'

import geomG from '../../data/gemeenten4326-2023_v1.json'
import geomP from '../../data/provincies.json'

export type FilterTerm =
  | 'landcode'
  | 'provinciecode'
  | 'gemeentecode'
  | 'wijkcode'
  | 'buurtcode'

export interface levelElement {
  state: number
  admin: 'Land' | 'Provincie' | 'Gemeente' | 'Wijk' | 'Buurt'
  filterTerm: FilterTerm
  filterNaam: string
  filterCode: string
  highlightTerm: 'provinciecode' | 'gemeentecode' | 'wijkcode' | 'buurtcode'
  data: string | FeatureCollection
  highlightCode: string
  highlightNaam: string
  active: boolean
  termnaam: 'provincienaam' | 'gemeentenaam' | 'wijknaam' | 'buurtnaam'
}

interface AppContextProps {
  levelsAll: Array<levelElement>
  hoverName: string
  activeAdminState: number
  center: number[]
  colorAttribute: boolean
  setCenter: Dispatch<SetStateAction<AppContextProps['center']>>
  setHoverName: Dispatch<SetStateAction<AppContextProps['hoverName']>>
  setNextName: (newnaam: string, newcode: string, state: number) => void
  setActiveState: Dispatch<SetStateAction<AppContextProps['activeAdminState']>>
  setEmptyName: (state: number) => void
  setAllNames: (state: number, result: string) => void
  setINITName: Dispatch<SetStateAction<void>>
  setColorAttribute: Dispatch<SetStateAction<AppContextProps['colorAttribute']>>
}

const AppContext = createContext<AppContextProps>({} as AppContextProps)

const AppContextProvider = ({ children }) => {
  const [levelsAll, changeLevelsAll] = useState<levelElement[]>([
    {
      state: 0,
      admin: 'Land',
      filterTerm: 'landcode',
      filterNaam: '',
      filterCode: '',
      highlightTerm: 'provinciecode',
      data: geomP as FeatureCollection,
      highlightCode: '',
      highlightNaam: 'Nederland',
      active: true,
      termnaam: 'provincienaam',
    },
    {
      state: 1,
      admin: 'Provincie',
      filterTerm: 'provinciecode',
      filterNaam: '',
      filterCode: '',
      highlightTerm: 'gemeentecode',
      highlightNaam: '',
      highlightCode: '',
      data: geomG as FeatureCollection,
      active: false,
      termnaam: 'gemeentenaam',
    },
    {
      state: 2,
      admin: 'Gemeente',
      filterTerm: 'gemeentecode',
      filterNaam: '',
      filterCode: '',
      highlightTerm: 'wijkcode',
      highlightNaam: '',
      highlightCode: '',
      data: `https://maps.commondatafactory.nl/maps/cbs?service=WFS&request=GetFeature&version=2.0.0&srsName=EPSG:4326&typename=cbs:wijk_admin_2023&outputFormat=geojson&pagingEnabled=true&propertyName=wijknaam,wijkcode,gemeentecode,gemeentenaam`,
      active: false,
      termnaam: 'wijknaam',
    },
    {
      state: 3,
      admin: 'Wijk',
      filterTerm: 'wijkcode',
      filterNaam: '',
      filterCode: '',
      highlightTerm: 'buurtcode',
      highlightNaam: '',
      highlightCode: '',
      data: `https://maps.commondatafactory.nl/maps/cbs?service=WFS&request=GetFeature&version=2.0.0&srsName=EPSG:4326&typename=cbs:buurt_admin_2023&outputFormat=geojson&pagingEnabled=true&propertyName=buurtcode,buurtnaam,wijknaam,wijkcode,gemeentecode,gemeentenaam&COORDINATE_PRECISION=1`,
      active: false,
      termnaam: 'buurtnaam',
    },
  ])
  const [activeAdminState, setActiveState] = useState(0)
  const [hoverName, setHoverName] = useState('')
  const [center, setCenter] = useState([4.308188, 52.087049])
  const [colorAttribute, setColorAttribute] = useState(false)

  const setINITName = () => {
    const newArray = [...levelsAll]
    newArray[0] = {
      ...levelsAll[0],
      active: true,
      filterNaam: 'Nederland',
      filterCode: 'NL01',
    }
    changeLevelsAll(newArray)
    setActiveState(0)
  }

  const setNextName = (
    newnaam: string,
    newcode: string,
    state: number
  ): void => {
    const newArray = [...levelsAll]
    if (state === 3 || state === 4) {
      newArray[3] = {
        ...levelsAll[3],
        active: true,
        highlightNaam: newnaam,
        highlightCode: newcode,
      }
    } else {
      newArray[state] = {
        ...levelsAll[state],
        active: false,
        highlightNaam: newnaam,
        highlightCode: newcode,
      }
      newArray[state + 1] = {
        ...levelsAll[state + 1],
        active: true,
        filterNaam: newnaam,
        filterCode: newcode,
        highlightNaam: '',
        highlightCode: '',
      }
    }
    changeLevelsAll(newArray)
  }
  const setAllNames = (state: number, result: string): void => {
    const newArray = [...levelsAll]
    for (let i = 0; i <= state; i++) {
      if (i <= 3) {
        newArray[i] = {
          ...levelsAll[i],
          active: state === i || i === 3,
          filterCode: result[i + i] ? result[i + i] : '',
          filterNaam: result[i + i + 1] ? result[i + i + 1] : '',
          highlightCode: result[i + i + 2] ? result[i + i + 2] : '',
          highlightNaam: result[i + i + 3] ? result[i + i + 3] : '',
        }
      }
    }
    changeLevelsAll(newArray)
  }
  const setEmptyName = (state: number): void => {
    const newArray = [...levelsAll]
    for (let i = state; i < 4; i++) {
      if (i === state) {
        newArray[i] = {
          ...levelsAll[i],
          active: true,
        }
      } else {
        newArray[i] = {
          ...levelsAll[i],
          active: false,
          highlightNaam: '',
          highlightCode: '',
          filterNaam: '',
          filterCode: '',
        }
      }
    }
    changeLevelsAll(newArray)
  }

  return (
    <AppContext.Provider
      value={{
        levelsAll: levelsAll,
        hoverName: hoverName,
        activeAdminState: activeAdminState,
        center: center,
        colorAttribute: colorAttribute,
        setCenter: setCenter,
        setHoverName: setHoverName,
        setNextName: setNextName,
        setActiveState: setActiveState,
        setEmptyName: setEmptyName,
        setAllNames: setAllNames,
        setINITName: setINITName,
        setColorAttribute: setColorAttribute,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export { AppContext, AppContextProvider }
