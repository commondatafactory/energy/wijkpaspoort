// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useContext, useEffect, useState } from 'react'
import styled from 'styled-components'

import { AppContext } from './GeoNavigatieContextAPI'

const StyledPanel = styled.div`
  grid-area: status;
  justify-self: stretch;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: column;
  margin-bottom: ${(div) => div.theme.tokens.spacing07};
  text-align: center;

  h3,
  p,
  a {
    margin: ${(p) => p.theme.tokens.spacing01};
  }

  @media screen and (max-width: 1024px) {
    margin-bottom: ${(div) => div.theme.tokens.spacing05};
  }
  a {
    font-size: 0.75rem;
  }
`

function InfoStatus() {
  const {
    activeAdminState,
    levelsAll,
    setCenter,
    center,
    setAllNames,
    setActiveState,
    setINITName,
  } = useContext(AppContext)
  // const [previousName, setPreviousName] = useState('')
  const [current, setCurrent] = useState('')

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    if (searchParams.has('admin')) {
      const adminCode = searchParams.get('admin')

      if (adminCode === 'NL01') {
        setActiveState(0)
        setAllNames(0, ['NL01', 'Nederland'])
      } else {
        const newAdminCode = adminCode.match('GM')
          ? adminCode.substr(2, 5)
          : adminCode

        fetch(
          `https://api.pdok.nl/bzk/locatieserver/search/v3_1/free?rows=1&fq=type:(provincie%20OR%20gemeente%20OR%20wijk%20OR%20buurt)&q=${encodeURIComponent(
            newAdminCode
          )}`
        )
          .then((response) =>
            response.ok ? response : Error(response.statusText)
          )
          .then((response) =>
            response.json().then((data) => {
              const geocodeResult = data.response.docs[0]
              setCenter([
                parseFloat(geocodeResult.centroide_ll.substr(6, 9)),
                parseFloat(geocodeResult.centroide_ll.substr(16, 26)),
              ])
              if (geocodeResult) {
                const result = [
                  'NL01',
                  'Nederland',
                  geocodeResult.provinciecode,
                  geocodeResult.provincienaam,
                  'GM' + geocodeResult.gemeentecode,
                  geocodeResult.gemeentenaam,
                  geocodeResult.wijkcode,
                  geocodeResult.wijknaam,
                  geocodeResult.buurtcode,
                  geocodeResult.buurtnaam,
                ]
                // Set state nr
                const types = ['land', 'provincie', 'gemeente', 'wijk', 'buurt']
                const state = types.indexOf(geocodeResult.type)
                setAllNames(state, result)
                setActiveState(state)
              } else {
                setAllNames(0, ['NL01', 'Nederland'])
                setActiveState(0)
              }
            })
          )
          .catch(function () {
            console.error(
              'Oeps, er ging iets fout met het ophalen van de data bij PDOK. \nMogelijk is er een storing bij de services  \n Blijven er problemen: vip@vng.nl \n \nprobeer het nog een keer! '
            )
            setAllNames(0, ['NL01', 'Nederland'])
            setActiveState(0)
          })
      }
    } else {
      setINITName()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    if (activeAdminState <= 3) {
      // eslint-disable-next-line
      searchParams.set('admin', levelsAll[activeAdminState].filterCode)
    } else if (activeAdminState === 4) {
      searchParams.set('admin', levelsAll[3].highlightCode)
    }
    let newurl = ''
    newurl =
      window.location.origin +
      window.location.pathname +
      '?' +
      searchParams.toString() +
      window.location.hash // HASH achteraan!
    window.history.pushState(newurl, '', newurl)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeAdminState, levelsAll[3].highlightCode])

  useEffect(() => {
    // let completeName = ''
    if (activeAdminState <= 3) {
      // eslint-disable-next-line
      setCurrent(levelsAll[activeAdminState])
    } else if (activeAdminState === 4) {
      setCurrent(levelsAll[3])
    }

    // levelsAll.forEach((elem) => {
    //   if (activeAdminState >= elem.state) {
    //     completeName += elem.filterNaam + ' | '
    //   }
    // })
    // if (activeAdminState === 4) {
    //   completeName += levelsAll[3].highlightNaam
    // }
    // setPreviousName(completeName)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [current, activeAdminState, levelsAll[3].highlightNaam])

  return (
    <StyledPanel>
      <h3>
        {activeAdminState === 4
          ? `Buurt ${current.highlightCode} ${current.highlightNaam}`
          : `${current.admin} ${current.filterCode} ${current.filterNaam}`}
      </h3>
      <small>Geometrie bron: 2023 CBS</small>
      <a
        title="Bekijk de kaart in de DEGO viewer"
        target="blank"
        href={`https://dego.vng.nl/?layer=layer1&tab=gebouw&label=topo#14/${center[1]}/${center[0]}`}
      >
        Bekijk gedetailleerde kaart in de DEGO viewer{' '}
        <span>
          {' '}
          <img
            alt="External link"
            title="Bekijk de kaart in de DEGO viewer, externe link"
            className="icon"
            src="/img/external-link-line.svg"
            style={{ height: '18px', width: '18px' }}
          />
        </span>
      </a>
    </StyledPanel>
  )
}
export default InfoStatus
