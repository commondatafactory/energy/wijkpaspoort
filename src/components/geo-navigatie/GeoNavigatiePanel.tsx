// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, {
  Dispatch,
  FC,
  SetStateAction,
  useContext,
  useEffect,
} from 'react'

import { AppContext, FilterTerm } from './GeoNavigatieContextAPI'
import * as Styled from './GeoNavigatiePanel.styled'
import InfoStatus from './GeoNavigatieStatus'
import GeoNavMaps from './GeoNavMaps'

interface GeoNavigatieAdminProps {
  setCurrentActiveAdminCode: Dispatch<SetStateAction<string>>
  setCurrentActiveAdminTerm: Dispatch<SetStateAction<FilterTerm>>
}

export const GeoNavigatieAdmin: FC<GeoNavigatieAdminProps> = ({
  setCurrentActiveAdminCode,
  setCurrentActiveAdminTerm,
}) => {
  const { activeAdminState, levelsAll, hoverName } = useContext(AppContext)

  useEffect(() => {
    if (activeAdminState <= 3) {
      setCurrentActiveAdminCode(levelsAll[activeAdminState].filterCode)
      setCurrentActiveAdminTerm(levelsAll[activeAdminState].filterTerm)
    } else if (activeAdminState === 4) {
      setCurrentActiveAdminCode(levelsAll[3].highlightCode)
      setCurrentActiveAdminTerm(levelsAll[3].highlightTerm)
    }
  }, [
    activeAdminState,
    levelsAll,
    setCurrentActiveAdminCode,
    setCurrentActiveAdminTerm,
  ])

  return (
    <Styled.MainNavPanel
      role="region"
      aria-label="Navigatie paneel voor het selecteren van een gemeente, wijk of buurt waarover de gegevens worden getoont."
    >
      <Styled.NavigationBar />
      <Styled.HoverName>{hoverName}</Styled.HoverName>
      <GeoNavMaps />
      <InfoStatus />
    </Styled.MainNavPanel>
  )
}

export default GeoNavigatieAdmin
