// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Alert, Alert as ImportAlert } from '@commonground/design-system'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

import { useIsMobile } from '../hooks/use-is-mobile'
import { notificationsData } from '../notifications'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  z-index: 50;

  @media screen and (min-width: 1024px) {
    gap: 1rem;
    /* left: ${(p) => p.theme.tokens.spacing05};
    top: ${(p) => p.theme.tokens.spacing05}; */
    width: 23rem;
    position: absolute;
  }
`

const StyledAlert = styled(ImportAlert)`
  box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;

  > * > .description {
    display: block;
    margin-top: 0.5rem;
  }
`

const Header = styled.span`
  align-items: center;
  display: flex;

  > span {
    font-weight: bold;
  }

  > small {
    margin: 0 8px 0 auto;
    font-size: 0.9rem;
  }

  > img {
    cursor: pointer;
  }
`

const Buttons = styled.span`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const addMonths = (numOfMonths, date = new Date()) => {
  date.setMonth(date.getMonth() + numOfMonths)
  return date
}

const filterNotifications = (notifications) =>
  notifications.reduce((acc, update) => {
    // Filter when update date has expired
    const currentDate = new Date()
    const updateDate = new Date(update.date)
    if (currentDate.getTime() > addMonths(3, updateDate).getTime()) {
      return acc
    }

    // Filter when set to not show again
    if (window.localStorage.getItem(`notification-${update.date}`)) {
      return acc
    }

    return [...acc, { ...update, index: acc.length + 1 }]
  }, [])

const validateNotifications = (notifications) =>
  notifications.forEach((notification, i) => {
    if (
      notifications.some(
        (update, _i) => i !== _i && update.date === notification.date
      )
    ) {
      throw new Error(
        `Notifications can't have same exact date: ${notification.date}`
      )
    }
  })

export function Notifications() {
  const isMobile = useIsMobile()

  const [notifications, setNotifications] = useState([])

  useEffect(() => {
    const notifications = filterNotifications(notificationsData)
    validateNotifications(notifications)

    setNotifications(notifications)
  }, [])

  const handleOnClose = (date) => {
    setNotifications(notifications.filter((update) => update.date !== date))
  }

  const handleOnCloseIndefinitely = (date) => {
    window.localStorage.setItem(`notification-${date}`, 'hidden')
    handleOnClose(date)
  }

  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }

  return (
    <Container>
      {notifications.slice(0, isMobile ? 1 : 2).map((update) => (
        <StyledAlert key={update.date} variant="info">
          <Header>
            <span>
              {update.title ||
                `Update ${new Date(update.date).toLocaleDateString(
                  'nl-NL',
                  dateOptions
                )}`}
            </span>
            <small>
              {update.index}/{notifications.length}
            </small>
            <img
              alt=""
              width="24"
              height="24"
              src="/img/close.svg"
              onClick={() => handleOnClose(update.date)}
            />
          </Header>

          <span className="description">{update.description}</span>

          <Buttons>
            {update.updateUrl && (
              <Alert.ActionButton
                as="a"
                href={update.updateUrl}
                target="_blank"
              >
                Volledige update bekijken
                <img
                  alt=""
                  width="16"
                  height="16"
                  src="/img/external-link.svg"
                  onClick={() => handleOnClose(update.date)}
                />
              </Alert.ActionButton>
            )}
            <Alert.ActionButton
              onClick={() => handleOnCloseIndefinitely(update.date)}
            >
              Niet meer tonen
            </Alert.ActionButton>
          </Buttons>
        </StyledAlert>
      ))}
    </Container>
  )
}
