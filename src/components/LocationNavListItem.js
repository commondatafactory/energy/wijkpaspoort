// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { func, number, shape } from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const LocationNavList = styled.li`
  cursor: pointer;
  padding: var(--spacing04);
  padding-left: var(--spacing06);
  list-style-type: none;
  border-top: dotted 0.5px var(--colorPaletteGray500);

  .result-selected {
    font-weight: bold;
  }
  LocationNavLister input[type='text'] {
    padding: var(--spacing03);
  }

  @media screen and (max-width: 768px) {
    .control-container input[type='text'] {
      padding: var(--spacing02);
    }

    .control-container {
      height: 10vh;
    }
    .location {
      margin: 0;
      padding: var(--spacing02);
    }
  }
`

// Show the list of possible results in the navigation bar, and make selected option bold
class LocationNavListItem extends React.Component {
  render() {
    return (
      <LocationNavList
        className={
          this.props.cursorPosition === this.props.i
            ? 'result-selected'
            : 'result-item'
        }
        data-testid="resultlistitem"
        key={this.props.item.id}
        value={this.props.item.id}
        onClick={(e) => this.props.handleClick(this.props.item, e)}
      >
        {this.props.item.weergavenaam}
      </LocationNavList>
    )
  }
}

LocationNavListItem.propTypes = {
  cursorPosition: number,
  i: number,
  item: shape(),
  handleClick: func,
}

export default LocationNavListItem
