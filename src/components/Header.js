// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import styled from 'styled-components'

import LocationNav from './LocationNav'

const StyledHeader = styled.div`
  background-color: ${(div) => div.theme.tokens.colorBackground};
  grid-row: 1;
  grid-column: 1 / span 1;
  align-self: stretch;
  padding-left: ${(div) => div.theme.tokens.spacing07};
  padding-right: ${(div) => div.theme.tokens.spacing07};
  display: flex;
  flex-direction: column;
`

const Title = styled.h1`
  display: flex;
  justify-items: space-between;
  align-items: center;
  margin: 0px !important;
  font-weight: 400;
  font-size: 18px;
  color: ${(h1) => h1.theme.tokens.colorPaletteGray900};
  /* @media screen and (max-width: 768px){
    margin: ${(div) => div.theme.tokens.spacing03};
  } */
  img {
    margin: ${(div) => div.theme.tokens.spacing05};
    margin-left: 0;
    @media screen and (max-width: 768px) {
      margin: ${(div) => div.theme.tokens.spacing03};
    }
  }
`

function Header() {
  return (
    <StyledHeader>
      <Title>
        <img src="/img/logo.svg" height="40" alt="cdf logo" />
        DEGO Wijkpaspoort
      </Title>
      <LocationNav />
    </StyledHeader>
  )
}
export default Header
