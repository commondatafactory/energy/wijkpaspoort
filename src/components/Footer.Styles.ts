// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const FooterContainer = styled.div`
  grid-row: 3 / span 1;
  grid-column: 1 / span 1;
  justify-self: stretch;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16);
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  padding: ${(div) => div.theme.tokens.spacing07};
  overflow: hidden;

  background-image: url('/kadaster_rotate.png');
  background-size: contain;
  Button {
    padding: ${(div) => div.theme.tokens.spacing03};
  }
  a {
    text-decoration: none;
  }

  @media screen and (max-width: 1024px) {
    Button {
      font-size: 0.85rem;
      padding: ${(div) => div.theme.tokens.spacing03};
    }
  }
`
