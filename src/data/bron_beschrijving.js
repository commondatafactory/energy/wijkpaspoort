// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

const sources = {
  woningvoorraad: {
    1: {
      title: 'Verdeling Woningvoorraad : Dataset Woningvoorraad Kadaster',
      aggregatie: 'Totaal aantal panden',
      jaar: 'oktober 2024',
      url: '',
      beschrijving: 'Pand Bezit, Bouwjaar, Type',
    },
  },
  verbruik: {
    1: {
      title:
        "CBS Statline: Energieverbruik particuliere woningen; woningtype en regio's",
      aggregatie: 'Land, Provincies',
      jaar: 2023,
      url: ' https://opendata.cbs.nl/statline/#/CBS/nl/dataset/81528NED/table?ts=1598523217901',
      beschrijving: 'Energieverbruik 2023',
    },
    2: {
      title: 'CBS Statline: Regionale kerncijfers',
      aggregatie: 'Land, Provincies',
      jaar: 2023,
      url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/70072NED/table?dl=699B7',
      beschrijving: 'Kerncijfers 2023',
    },
    3: {
      title: 'CBS Statline, Kerncijfers wijken en buurten 2023',
      aggregatie: 'Gemeenten, Wijken en Buurten',
      jaar: 2023,
      url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/85618NED/table?dl=699B7',
      beschrijving: 'Kerncijfers 2023',
    },
  },
  sociaal: {
    1: {
      title: 'CBS Statline, Kerncijfers wijken en buurten 2023',
      aggregatie: 'Gemeenten, Wijken en Buurten',
      jaar: 2023,
      url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/85618NED/table?dl=699B7',
      beschrijving: 'Kerncijfers 2023',
    },
  },
  eklasse: {
    1: {
      title: 'RVO energieklasses',
      aggregatie: 'Energieklasses',
      jaar: 2024,
      url: 'https://commondatafactory.nl/docs/api/energie',
      beschrijving: `Verdeling (som) van de voorkomende Energie labels en woning types uit de Energie Klassen binnen een administratieve aggregatie. De energie klasses bevatten zowel woningbouw als utiliteits bouw. <a href='https://www.ep-online.nl/PublicData' target='_new'>Orginele bron</a> (RVO).`,
    },
  },
  utiliteit: {
    1: {
      title: 'Som van de Gebruiksdoel(en) in een verblijfsobject uit de BAG',
      jaar: 2024,
      aggregatie:
        'De som aggregatie gaat over alle verblijfsobjecten binnen een gemeente, wijk, buurt.',
      url: 'https://gemeente.incijfers.nl/info/BAG_ABF.html',
      beschrijving:
        'Som van de categorisering gebruiksdoel over alle verblijfsbojecten binnen een administratieve aggregatie. Let op, per verblijfsobject kunnen er meer dan 1 gebruiksdoelen zijn. Deze som geeft het totaal over alle voorkomende gebruiksdoelen weer.',
    },
  },

  hvi: {
    1: {
      title:
        'CBS, Woningen; hoofdverwarmingsinstallaties, wijken en buurten, 2022',
      jaar: 2022,
      aggregatie: 'Nederland, Gemeenten, wijken en buurten',
      url: 'https://opendata.cbs.nl/#/CBS/nl/dataset/85677NED/table?ts=1699434515286',
      beschrijving:
        'Telling van de hoofdverwarmingsinstallatie van de woningvoorraad, ingedeeld van gemeente- tot buurtniveau.',
    },
    2: {
      title: 'CBS, "Woningen; hoofdverwarmingsinstallaties, regio',
      jaar: 2021,
      aggregatie: 'Provincies',
      url: 'https://opendata.cbs.nl/#/CBS/nl/dataset/84948NED/table?ts=1677593972364',
      beschrijving:
        'Telling van de hoofdverwarmingsinstallatie van de woningvoorraad, ingedeeld op provincieniveau.',
    },
  },
  levensloop: {
    1: {
      title: 'CBS, Levensloopbestendig wonen',
      jaar: 2022,
      aggregatie: 'Gemeenten, wijken en buurten',
      url: 'https://www.cbs.nl/nl-nl/maatwerk/2023/05/levensloopbestendig-wonen-2022',
      beschrijving:
        'Eenmalige dataset, gemaakt door het CBS in opdracht van de VNG.',
    },
  },
  armoede: {
    1: {
      title: 'CBS, Monitor Energiearmoede',
      jaar: 2021,
      aggregatie: 'Gemeenten, wijken en buurten (van 2023)',
      url: 'https://www.cbs.nl/nl-nl/maatwerk/2023/47/monitor-energiearmoede-2021',
      beschrijving:
        'Energiearmoede gaat over huishoudens die die te maken hebben met een laag inkomen in combinatie met een te hoge energierekening en/of een woning van lage energetische kwaliteit. \n\n De Nederlandse populatie particuliere huishoudens bij aanvang van het verslagjaar vormt de basis van de populatie. Een deel van de bevolking blijft echter in de monitor Energiearmoede buiten beeld vanwege conceptuele of praktische redenen: Huishoudens die een woning delen (bijvoorbeeld woongroepen). Huishoudens zonder bekend of volledig jaarinkomen. Huishoudens in een woning waarvan het energieverbruik onbekend is. Huishoudens die wonen in een verblijfsobject dat volgens de BAG geen woonfunctie heeft, zoals bedrijven of stand- en ligplaatsen. \n Wij tonen hier alleen de indicator Lihelek. Voor meer indicatoren zie DEGO of het CBS.',
    },
  },
}

export default sources
