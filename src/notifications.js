// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const notificationsData = [
  {
    // "Voorbeeld notificatie - wordt niet getoond",
    title: 'Eigen titel in plaats van een datum', // titel is optioneel
    date: '2000-06-15T00:00:00.000Z',
    description: 'Beschrijving',
    updateUrl: 'https://voorbeeld.nl',
  },
  // Sorteer updates van nieuw naar oud
  {
    date: '2023-02-28T00:00:00.000Z',
    title:
      'Data update: Verdeling Hoofdverwarmingsinstallatie woningen toegevoegd!',
    description:
      'Een nieuwe data visualisatie over de type hoofdverwarmingsinstallaties van woningen is nu toegevoegd. Data uit 2019',
  },
  {
    date: '2022-09-12T00:00:00.000Z',
    title: 'Data update: Nieuwe Energie Label klasse',
    description:
      'Energie Label klasse juli 2022 nu beschikbaar. Daarbij is de ondersteuning voor energie labels, definitief en voorlopig, vervallen.',
  },
  {
    date: '2022-08-18T00:00:00.000Z',
    description: `Foutieve waarden voor de gemiddelde WOZ waarde in de export tabel voor het Kadaster wijkpaspoort zijn gecorrigeerd.`,
  },
  {
    date: '2023-11-08T00:00:00.000Z',
    title: 'Data update: Veel CBS statline bronnen geupdate naar 2021 en 2022',
    description:
      'Energie verbruik van 2019 naar 2021. \n Hoofdverwarmingsinstallatie van 2021 naar 2022. \n Sociale kenmerken 2019 naar 2021. \n Woningwaarde 2019 naar 2021. \n ',
  },
  // {
  //   date: '2021-07-11T00:00:00.000Z',
  //   description: `De getallen van de Woningvoorraad zijn verbeterd. Deze waren afgerond in hele getallen, nu geven ze de exacte waarden aan. De getallen in de export waren al wel juist.`,
  // },
  // {
  //   date: '2022-07-14T00:00:00.000Z',
  //   description: `De Utiliteitsbouw data is verbeterd. Het paneel 'Aanwezige Utiliteitsbouw' bevat nu betere en nieuwere cijfers. De getallen kunnen hoger uitvallen dan hiervoor.`,
  // },
]
