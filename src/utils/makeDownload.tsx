// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { sum } from './helper'

export const makeTableKadasterV2 = function (
  dataSet: any,
  activeAdminState: number,
  current: any
) {
  const energieAplus = ['A', 'A+', 'A++', 'A+++', 'A+++++']

  const gemiddeldInkomen = Math.round(
    100 -
      dataSet.k_40HuishoudensMetLaagsteInkomen -
      dataSet.k_20HuishoudensMetHoogsteInkomen
  )

  const totaalEnergieAPlus = sum(dataSet, energieAplus)
  const downloadDataSet = {
    Gemeentenaam: activeAdminState === 2 ? current.naam : '',
    Gemeentecode: activeAdminState === 2 ? current.code : '',
    Wijknaam: activeAdminState === 3 ? current.naam : '',
    Wijkcode: activeAdminState === 3 ? current.code : '',
    Buurtnaam: activeAdminState === 4 ? current.naam : '',
    Buurtcode: activeAdminState === 4 ? current.code : '',

    Woningvoorraad_Totaal: dataSet.wv_totaal_WoningVoorraad,
    Woningvoorraad_Particulier: dataSet.wv_totaal_TypeEigenaar_ParticulierBezit,
    Woningvoorraad_Woningcorporaties:
      dataSet.wv_totaal_TypeEigenaar_CorporatieBezit,
    Woningvoorraad_Overighuur: dataSet.wv_totaal_TypeEigenaar_OverigHuurBezit,
    Woningvoorraad_Overig: dataSet.wv_totaal_TypeEigenaar_OverigBezit,

    Bouwperiode_voor_1946: dataSet.wv_aantal_Bouwperiode_Voor_1946,
    Bouwperiode_1946_1974: dataSet.wv_aantal_Bouwperiode_1946_1974,
    Bouwperiode_1975_1991: dataSet.wv_aantal_Bouwperiode_1975_1991,
    Bouwperiode_1992_2006: dataSet.wv_aantal_Bouwperiode_1992_2006,
    Bouwperiode_na_2006: dataSet.wv_aantal_Bouwperiode_2006_later,

    Energielabel_A_of_beter: totaalEnergieAPlus,
    Energielabel_B: dataSet.B,
    Energielabel_C: dataSet.C,
    Energielabel_D: dataSet.D,
    Energielabel_E: dataSet.E,
    Energielabel_F: dataSet.F,
    Energielabel_G: dataSet.G,
    Geen_Energielabel: dataSet.overigLabel,

    Appartement: dataSet.wv_aantal_TypeWoning_Appartement,
    Tussenwoning: dataSet.wv_aantal_TypeWoning_Tussenwoning,
    Hoekwoning: dataSet.wv_aantal_TypeWoning_Hoekwoning,
    '2-onder-1-kap': dataSet.wv_aantal_TypeWoning_TweeOnderEenKap,
    Vrijstaand: dataSet.wv_aantal_TypeWoning_VrijstaandeWoning,
    Overig: dataSet.wv_aantal_TypeWoning_Overig,

    Gemiddelde_WOZ: dataSet.GemiddeldeWOZWaardeVanWoningen,
    Gemiddeld_Elektriciteitsverbruik: dataSet.Gemiddeld_Elektriciteitsverbruik,
    Gemiddeld_Gasverbruik: dataSet.Gemiddeld_Gasverbruik,
    Gemiddeld_Elektriciteitsverbruik_Appartement:
      dataSet.Gemiddeld_Elektriciteitsverbruik_Appartement,
    Gemiddeld_Elektriciteitsverbruik_Tussenwoning:
      dataSet.Gemiddeld_Elektriciteitsverbruik_Tussenwoning,
    Gemiddeld_Elektriciteitsverbruik_Hoekwoning:
      dataSet.Gemiddeld_Elektriciteitsverbruik_Hoekwoning,
    Gemiddeld_Elektriciteitsverbruik_2_onder_1_kap:
      dataSet.Gemiddeld_Elektriciteitsverbruik_2_onder_1_kap,
    Gemiddeld_Elektriciteitsverbruik_Vrijstaand:
      dataSet.Gemiddeld_Elektriciteitsverbruik_Vrijstaand,
    Gemiddeld_Gasverbruik_Appartement:
      dataSet.Gemiddeld_Gasverbruik_Appartement,
    Gemiddeld_Gasverbruik_Tussenwoning:
      dataSet.Gemiddeld_Gasverbruik_Tussenwoning,
    Gemiddeld_Gasverbruik_Hoekwoning: dataSet.Gemiddeld_Gasverbruik_Hoekwoning,
    Gemiddeld_Gasverbruik_2_onder_1_kap:
      dataSet.Gemiddeld_Gasverbruik_2_onder_1_kap,
    Gemiddeld_Gasverbruik_Vrijstaand: dataSet.Gemiddeld_Gasverbruik_Vrijstaand,

    BAG_Onderwijsfunctie: dataSet.onderwijsfunctie,
    BAG_Kantoorfunctie: dataSet.kantoorfunctie,
    BAG_Winkelfunctie: dataSet.winkelfunctie,
    BAG_Zorgfunctie: dataSet.gezondheidszorgfunctie,
    BAG_Industriefunctie: dataSet.industriefunctie,
    BAG_Sportfunctie: dataSet.sportfunctie,
    BAG_Bijeenkomstfunctie: dataSet.bijeenkomstfunctie,
    BAG_Logiesfunctie: dataSet.logiesfunctie,
    BAG_Celfunctie: dataSet.celfunctie,
    BAG_Overigfunctie: dataSet['overige gebruiksfunctie'],

    'Aantal huishoudens': dataSet.HuishoudensTotaal,
    'Aantal inwoners totaal': dataSet.AantalInwoners,
    'Tot 25 jaar': dataSet.k_0Tot15Jaar + dataSet.k_15Tot25Jaar,
    '25 tot 45 jaar': dataSet.k_25Tot45Jaar,
    '45 tot 65 jaar': dataSet.k_45Tot65Jaar,
    '65 jaar en ouder': dataSet.k_65JaarOfOuder,

    Energiearmoede_LIHLEK: dataSet.ea_LIHLEK,
    '40% laagste inkomens': dataSet.k_40HuishoudensMetLaagsteInkomen,
    'Gemiddeld inkomen': gemiddeldInkomen,
    '20% hoogste inkomens': dataSet.k_20HuishoudensMetHoogsteInkomen,
  }
  return downloadDataSet
}
