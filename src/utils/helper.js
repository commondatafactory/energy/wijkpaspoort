// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as d3 from 'd3'

export const stringFillup = function (string, nr) {
  if (string.length < nr) {
    string += ' '.repeat(nr - string.length)
  }
  return string
}

function thousandFormatter(amount) {
  const delimiter = '.'
  let number = typeof amount !== 'undefined' && amount > 0 ? String(amount) : ''
  // Decimals with comma
  number = number.replace('.', ',')
  // thousands with dot
  number = number
    .replace(
      new RegExp(
        '^(\\d{' + (number.length % 3 ? number.length % 3 : 0) + '})(\\d{3})',
        'g'
      ),
      '$1 $2'
    )
    .replace(/(\d{3})+?/gi, '$1 ')
    .trim()
  number = number.replace(/\s/g, delimiter)
  return number
}

export function decide(een, twee, unit) {
  if (een === 0) {
    return 0
  } else if (een) {
    return thousandFormatter(een) + ' ' + unit
  } else if (twee) {
    return thousandFormatter(twee) + ' ' + unit
  } else {
    return 'Geen data'
  }
}

export function wrapVert(text, width) {
  text.each(function () {
    const text = d3.select(this)

    const words = String(text.text()).split(/\s+/).reverse()

    let word
    let line = []
    let lineNumber = 0
    const lineHeight = 1.1 // ems
    const y = text.attr('y')
    const dy = parseFloat(text.attr('dy'))
    let tspan = text
      .text(null)
      .append('tspan')
      .attr('x', 0)
      .attr('y', y)
      .attr('dy', dy + 'em')
    while ((word = words.pop())) {
      line.push(word)
      tspan.text(line.join(' '))
      if (tspan.node().getComputedTextLength() > width) {
        line.pop()
        tspan.text(line.join(' '))
        line = [word]
        tspan = text
          .append('tspan')
          .attr('x', 0)
          .attr('y', y)
          .attr('dy', ++lineNumber * lineHeight + dy + 'em')
          .text(word)
      }
    }
  })
}

export function wrapHorz(text, width) {
  text.each(function () {
    const text = d3.select(this)

    const words = String(text.text()).split(/\s+/).reverse()

    let word
    let line = []
    let lineNumber = 0
    const lineHeight = 1 // ems
    const x = text.attr('x')
    const y = text.attr('y')
    const dy = parseFloat(text.attr('dy'))
    let tspan = text
      .text(null)
      .append('tspan')
      .attr('x', x)
      .attr('y', y)
      .attr('dy', dy + 'em')
    while ((word = words.pop())) {
      line.push(word)
      tspan.text(line.join(' '))
      if (tspan.node().getComputedTextLength() > width) {
        line.pop()
        tspan.text(line.join(' '))
        line = [word]
        tspan = text
          .append('tspan')
          .attr('x', x)
          .attr('y', y)
          .attr('dy', ++lineNumber * lineHeight + dy + 'em')
          .text(word)
      }
    }
  })
}

export function editELabelData(dataSet, naam) {
  return {
    [naam + '_Overig']: dataSet[0] ? parseFloat(dataSet[0].count) : 0,
    [naam + '_A']: dataSet[1] ? parseFloat(dataSet[1].count) : 0,
    [naam + '_B']: dataSet[2] ? parseFloat(dataSet[2].count) : 0,
    [naam + '_C']: dataSet[3] ? parseFloat(dataSet[3].count) : 0,
    [naam + '_D']: dataSet[4] ? parseFloat(dataSet[4].count) : 0,
    [naam + '_E']: dataSet[5] ? parseFloat(dataSet[5].count) : 0,
    [naam + '_F']: dataSet[6] ? parseFloat(dataSet[6].count) : 0,
    [naam + '_G']: dataSet[7] ? parseFloat(dataSet[7].count) : 0,
  }
}

export function sum(obj, list) {
  var sum = 0
  list.forEach((element) => {
    sum += parseFloat(obj[element]) || 0
  })
  return sum
}
