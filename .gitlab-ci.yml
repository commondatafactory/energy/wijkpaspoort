image: node:15.11.0-alpine3.13

stages:
  # - lint_typecheck_test
  - build_and_push
  - review
  - deploy

cache:
  key: '${CI_PROJECT_PATH}'
  paths:
    - ${NPM_CACHE_DIR}
    - node_modules/
    - .next/cache/

variables:
  WIJKPASPOORT_SHA_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  WIJKPASPOORT_REF_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  REVIEW_NAMESPACE: cdf-$CI_ENVIRONMENT_SLUG
  REVIEW_BASE_DOMAIN: nlx.reviews
  NPM_CACHE_DIR: ${CI_PROJECT_DIR}/.npm-cache

  # Lint, typecheck & test:
  #   stage: lint_typecheck_test
  #   before_script:
  #     - yarn install --frozen-lockfile --cache ${NPM_CACHE_DIR} --prefer-offline --no-progress --color=false --quiet
  #   script:
  #     # - yarn lint
  #     # - yarn type-check
  #     - yarn test
  #   coverage: /All\sfiles.*?\s+(\d+.\d+)/
  #   artifacts:
  #     expire_in: 1 month
  #     paths:
  #       - coverage
  #   needs: []

.build:
  image: docker:dind
  stage: build_and_push
  services:
    - docker:20.10.23-dind
  variables:
    DOCKER_TLS_CERTDIR: '/certs'
    NEXT_TELEMETRY_DISABLED: 1
  before_script:
    - apk add yarn jq
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - yarn install --frozen-lockfile --cache ${NPM_CACHE_DIR} --prefer-offline --no-progress --color=false --quiet
  only:
    - branches@commondatafactory/energy/wijkpaspoort
  # needs: ['Lint, typecheck & test']

Build and push WIJKPASPOORT:
  extends: .build
  script:
    - |
      yarn build && \
      find out -name "*.js" | xargs -I % sh -c 'gzip %' && \
      docker pull $CI_REGISTRY_IMAGE:latest && \
      docker build \
        --cache-from $CI_REGISTRY_IMAGE:latest \
        --tag $WIJKPASPOORT_SHA_TAG --tag $WIJKPASPOORT_REF_TAG . && \
      docker push $WIJKPASPOORT_SHA_TAG && \
      docker push $WIJKPASPOORT_REF_TAG
  except:
    - main

Build and push WIJKPASPOORT latest:
  extends: .build
  script:
    - |
      yarn build && \
      find out -name "*.js" | xargs -I % sh -c 'gzip %' && \
      docker pull $CI_REGISTRY_IMAGE:latest && \
      docker build \
        --tag $CI_REGISTRY_IMAGE:latest \
        --tag $WIJKPASPOORT_SHA_TAG --tag $WIJKPASPOORT_REF_TAG . && \
      docker push $WIJKPASPOORT_SHA_TAG && \
      docker push $WIJKPASPOORT_REF_TAG && \
      docker push $CI_REGISTRY_IMAGE:latest
  only:
    - main

WIJKPASPOORT Review:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    WIJKPASPOORT_REVIEW_URL: $CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
  before_script:
    - echo -e -n "https://$WIJKPASPOORT_REVIEW_URL" > wijkpaspoort_ci_environment_url.txt
    - git clone https://gitlab.com/commondatafactory/helm-charts/wijkpaspoort.git helm/wijkpaspoort
    - |
      cat >./helm/wijkpaspoort/overrides.yaml <<EOL
      wijkpaspoort:
        enabled: true
        image:
          tag: "${CI_COMMIT_SHORT_SHA}"
        replicaCount: 1
        ingress:
          hosts:
            - ${WIJKPASPOORT_REVIEW_URL}
          tls:
          - hosts:
            - ${WIJKPASPOORT_REVIEW_URL}
            secretName: wijkpaspoort-review-ingress-tls
      EOL

      cat ./helm/wijkpaspoort/overrides.yaml
  script:
    - |
      kubectl config use-context commondatafactory/ci/kubernetes-agents:review
      kubectl create namespace $REVIEW_NAMESPACE || true
      helm upgrade --install $REVIEW_NAMESPACE ./helm/wijkpaspoort \
        --namespace $REVIEW_NAMESPACE \
        --values ./helm/wijkpaspoort/values.yaml \
        --values ./helm/wijkpaspoort/overrides.yaml
  after_script:
    - echo -e -n "---> https://$WIJKPASPOORT_REVIEW_URL"
  environment:
    name: review/wijkpaspoort-$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    on_stop: Remove WIJKPASPOORT Review
    auto_stop_in: 1 week
  only:
    - branches@commondatafactory/energy/wijkpaspoort
  except:
    - develop
    - main
  artifacts:
    paths:
      - wijkpaspoort_ci_environment_url.txt
  needs: ['Build and push WIJKPASPOORT']

Remove WIJKPASPOORT Review:
  stage: review
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    GIT_STRATEGY: none
  script:
    - helm uninstall $REVIEW_NAMESPACE --namespace $REVIEW_NAMESPACE
    - kubectl delete namespace $REVIEW_NAMESPACE
  when: manual
  environment:
    name: review/wijkpaspoort-$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.$REVIEW_BASE_DOMAIN
    action: stop
  only:
    - branches@commondatafactory/energy/wijkpaspoort
  except:
    - develop
    - main

.deploy:
  extends: .build
  stage: deploy

Deploy Acceptation:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - git clone https://gitlab.com/commondatafactory/helm-charts/wijkpaspoort.git helm/wijkpaspoort
    - |
      cat >./helm/wijkpaspoort/overrides.yaml <<EOL
      wijkpaspoort:
        enabled: true
        image:
          tag: "${CI_COMMIT_SHORT_SHA}"
        replicaCount: 3
        ingress:
          hosts:
            - acc.wijkpaspoort.commondatafactory.nl
          tls:
          - hosts:
            - acc.wijkpaspoort.commondatafactory.nl
            secretName: acc-wijkpaspoort-cdf-nl-ingress-tls
      EOL
      cat ./helm/wijkpaspoort/overrides.yaml
  script:
    - |
      kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
      helm upgrade --install wijkpaspoort ./helm/wijkpaspoort \
        --namespace cdf-acc \
        --values ./helm/wijkpaspoort/values.yaml \
        --values ./helm/wijkpaspoort/overrides.yaml
  environment:
    name: acc
  only:
    - develop
  needs: ['Build and push WIJKPASPOORT']

Deploy Production:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - git clone https://gitlab.com/commondatafactory/helm-charts/wijkpaspoort.git helm/wijkpaspoort
    - |
      cat >./helm/wijkpaspoort/overrides.yaml <<EOL
      wijkpaspoort:
        enabled: true
        image:
          tag: "${CI_COMMIT_SHORT_SHA}"
        replicaCount: 3
        ingress:
          hosts:
            - wijkpaspoort.commondatafactory.nl
            - wijkpaspoort.vng.nl
          tls:
          - hosts:
            - wijkpaspoort.vng.nl
            secretName: wijkpaspoort-vng-nl-ingress-tls
          - hosts:
            - wijkpaspoort.commondatafactory.nl
            secretName: wijkpaspoort-cdf-nl-ingress-tls
      EOL
      cat ./helm/wijkpaspoort/overrides.yaml
  script:
    - |
      kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
      helm upgrade --install wijkpaspoort ./helm/wijkpaspoort \
        --namespace cdf-prod \
        --values ./helm/wijkpaspoort/values.yaml \
        --values ./helm/wijkpaspoort/overrides.yaml
  environment:
    name: prod
  only:
    - main
  needs: ['Build and push WIJKPASPOORT latest']
