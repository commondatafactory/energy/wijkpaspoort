// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

const nextConfig = {
  typescript: {
    ignoreBuildErrors: true,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
}

module.exports = nextConfig
