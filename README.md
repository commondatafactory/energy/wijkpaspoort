# Wijkpaspoort

Ontwerp zie:

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FA5oEHaPkjZTXXr5neTTewf%2FWijkpaspoort%3Fnode-id%3D0%253A1&chrome=DOCUMENTATION" allowfullscreen></iframe>

https://www.figma.com/file/A5oEHaPkjZTXXr5neTTewf/Wijkpaspoort?node-id=0%3A1

// '#0ba19f',
// '#0B71A1',
// '#262D30',
// '#BDBDBD',
// '#474E57',

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Developers

Read our [developer guide lines](./DEVELOPER.md)

## Available Scripts

In the project directory, you can run:

### `yarn`

To install all dependencies.

### `yarn dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

The result for on a static file server is available in the `out` directory.

### `yarn start`

Runs the production build.

You will need to run `yarn build` first (see above).
